gitmount(1)                          General Commands Manual                          gitmount(1)

NAME
       gitmount - GNU Interactive Tools - auto-mount script

SYNTAX
       gitmount device

DESCRIPTION
       gitmount  is  a  script  which allows you to mount any block device without specifying the
       file system type.  You may now insert the floppy in the drive and type   '  gitmount  fd0'
       and  the  first floppy will be mounted in the directory /mnt/fd0.  On Linux just press F11
       or F12.  You don't need to know the file system type anymore. The directories /mnt/fd0 and
       /mnt/fd1 are created if they don't exist.

BUGS
       Please send bug reports to:
       gnuit-dev@gnu.org

SEE ALSO
       gitfm(1) gitps(1) gitview(1) gitkeys(1) gitaction(1) gitrgrep(1) gitunpack(1)

AUTHORS
       Tudor Hulubei <tudor@cs.unh.edu>
       Andrei Pitis <pink@pub.ro>
       Ian Beckwith <ianb@erislabs.net> (Current maintainer)

                                                                                      gitmount(1)
