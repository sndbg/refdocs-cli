unsort - reorder files semi-randomly
Usage: unsort [-hvrpncmMz0l] [-s <integer>] [-f [strategy]] [file...]
	-h, --help               Print this message to stdout
	-v, --version            Print the program version
	-r, --random             Use a random permutation
	-f, --filenames[=<sub>]  Permute as if the input were filenames
	-S, --separator <sep>    Filename separator for -f
	-p, --heuristic          Use a heuristic permutation (default)
	-n, --identity           Do not change the order of lines
	-c, --concatenate        Concatenate input before shuffling
	-m, --merge              Merge input after shuffling in given order
	-M, --merge-random       Merge input after shuffling (default)
	-s, --seed <integer>     Seed the permutation
	-z, --zero-terminated    Use \0 line endings
	-0, --null               Use \0 line endings
	-l, --linefeed           Use \n line endings (default)
