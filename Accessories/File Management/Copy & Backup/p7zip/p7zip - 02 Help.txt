Usage: /usr/bin/p7zip [options] [--] [ name ... ]

Options:
    -c --stdout --to-stdout      output data to stdout
    -d --decompress --uncompress decompress file
    -f --force                   do not ask questions
    -k --keep                    keep original file
    -h --help                    print this help
    --                           treat subsequent arguments as file
                                 names, even if they start with a dash

