cdw(1)                               General Commands Manual                               cdw(1)

NAME
       cdw - front-end for cdrecord, mkisofs, growisofs, mkudffs and other tools

SYNOPSIS
       cdw [--help] [--version] [--enable-dvd-rp-dl] [--escdelay=X]

DESCRIPTION
       cdw  is  a front-end for command-line tools used for burning data CD and DVD discs and for
       related tasks. The tools are: cdrecord/wodim, mkisofs/genisoimage, growisofs, dvd+rw-medi‐
       ainfo,  dvd+rw-format,  xorriso.  You can also use cdw to rip tracks from your audio CD to
       raw audio files.  Limited support for copying content of CD and DVD discs to  image  files
       is  also  provided.  cdw  can  utilize md5sum program to verify correctness of writing ISO
       image to CD and DVD disc. cdw can be also used to create UDF image file using mkudffs.

       cdw uses ncurses library to build user-friendly interface and it can be used in UNIX  ter‐
       minal  window and in terminal emulator (like konsole, rxvt or gnome-console) in X environ‐
       ment.

       cdw supports ISO9660 file system for optical media: ISO9660 file system can be written  to
       stand-alone  .iso file, or burned to optical disc.  cdw also supports UDF file system: UDF
       file system can be written to stand-alone .udf file. Currently there  is  no  support  for
       burning  UDF file system directly to optical disc.  Other filesystem for optical media are
       not supported nor recognized.

SUPPORTED MEDIA
       Scope of supported features depends on which tools are installed  on  end  user's  system.
       Full list of supported media and actions is listed below.

       CD-AUDIO
              ripping audio tracks to raw audio files

       CD-R   writing  files  and  ISO images (single- and multi-session) with cdrecord/wodim and
              xorriso; copying content of first session to image file on your hard disc

       CD-RW  writing files and ISO images (single- and multi-session), erasing (erasing only TOC
              or  blanking  whole disc) with cdrecord/wodim and xorriso; copying content of first
              session to image file on your hard disc

       DVD-R, DVD+R
              writing files and ISO images (multi-session, but  without  closing  disc,  or  sin‐
              gle-session) with cdrecord/wodim, dvd+rw-tools and xorriso

       DVD-RW writing files and ISO images (multi-session or single-session); erasing, formatting
              - quick or slow (full) method; dvd+rw-tools only.  WARNING: multiple  blanking  may
              quickly render your DVD-RW unusable.

       DVD+RW writing  files  and  ISO  images  (both  writing to disc from scratch and appending
              data), without closing disc; blanking disc, but takes lots of time, and  you  don't
              need to do it anyway - you can always start writing to the same disc like you would
              write to blank disc. DVD+RW can be handled by both cdrecord/wodim and dvd+rw-tools.
              Notice that when using wodim you may encounter problems.

       DVD+R DL
              there  is  now partial support for DVD+R DL discs: cdw can burn ISO image and files
              in single session. User has to explicitly enable it by passing "--enable-dvd-rp-dl"
              command   line  argument  to  cdw;  cdw  can  handle  DVD+R  DL  discs  only  using
              dvd+rw-tools; this feature is very incomplete and possibly buggy.

              Dual-layer media other than DVD+R DL are  not  yet  supported.   DVD-RAM,  Blu-ray,
              HD-DVD discs are not supported.

MANUAL
       You  can write data discs either by using previously created ISO image file, or by select‐
       ing files from hard drive and writing them directly to disc.  cdw provides UI elements for
       both actions.

       First action before creating new ISO/UDF image or writing files to disc is selecting files
       from your hard drive. You can do this by selecting "Add files" in left-hand menu. You will
       be  presented  with simple file selector.  Use Up/Down arrows or Page Up/Page Down keys to
       move, Enter key to change current directory, Space key to  select  files  or  directories.
       Selected  items  will  be  added to list displayed in main window. Use Escape key to close
       file selector window. The selector window shows you all files and  directories  (including
       hidden  files)  in current directory. You will see file size information for every file on
       the list. You can also delete previously selected files from list: select  "Delete  files"
       from  the  menu  and  use  Up/Down arrow keys to move and Delete key to delete highlighted
       item. Use Escape key when you finish deleting files from list of selected files.

       Now you can select "Create image" from left-hand menu. cdw will display  a  wizard  window
       where  you can change most common options for creating ISO9660 or UDF image, and where you
       can select path to target image file on your hard disc.

       You can also select "Write files to disc" to write selected files to optical disc. If  you
       select  the  option, cdw will display a wizard dialog window, in which you can adjust most
       common options related to burning files to disc (for this function only ISO9660 file  sys‐
       tem is supported).

       If  you  want to write ISO image file to optical disc, use "Write image to disc" option in
       left hand menu. You will be presented with file picker dialog that allows  you  to  select
       existing  ISO image file from your hard disc. After that cdw will display a wizard window,
       where you can modify most common options available for burning a disc.

       cdw allows you to verify correctness of this operation by checking a digest (e.g. md5 sum)
       of ISO file and of track written to disc.  You can request this by checking "Verify write"
       checkbox that will appear in write wizard. Please note that this checkbox is visible  only
       when  you  are writing ISO file to empty CD or DVD disc. This function is still experimen‐
       tal. Make sure that a program for calculating digests is installed on  your  machine.  cdw
       supports following programs: md5sum sha1sum sha224sum sha256sum sha384sum sha512sum

       Another operation you can perform is erasing optical disc - CD-RW, DVD-RW (both Sequential
       Recording and Restricted Overwrite) or DVD+RW. You can do this by selecting  "Erase  disc"
       option in cdw left-hand menu. Please note that extensive erasing of DVD-RW may render your
       disc unusable. In case of CD-RW and DVD-RW you will be asked what  type  of  blanking  you
       want  to  perform.  You can choose fast mode or full mode. First one doesn't take too much
       time (in case of CD-RW it only erases table of content of your disc), second  one  can  be
       very time-consuming, depending on disc size and selected speed.

       When  you  will  attempt  to blank DVD-RW disc, blanking wizard will display dropdown that
       allows you to choose mode (format) of DVD-RW  disc:  Restricted  Overwrite  or  Sequential
       Recording.

       cdw  can  be  useful  when  you want to copy your data CD or DVD to ISO image on your hard
       drive. You do this by selecting "Read disc" option  from  left-hand  menu.  This  function
       allows  you  to  copy  first  track from your data CD or DVD. Reading second and following
       tracks from data discs, and reading discs written in mixed mode (one or more data tracks +
       audio tracks) are not supported.

       You  can  also use cdw to copy your audio CD to separate files (each track will be written
       to separate file). You do this by selecting "Read disc" option from menu. cdw  can't  con‐
       vert audio tracks to any popular audio format. The tracks are written to files in the same
       format as they appear on CD (2 channels, 44100 samples per second, 16 bit signed PCM, lit‐
       tle  endian (intel)). The file names have following name format: track_xx.raw (where xx is
       track number). You can convert raw track file to wav file using e.g. sox command:

       sox -c 2 -r 44100 -L -2 -s track_name.raw -t wav track_name.wav

       The last operation that cdw offers is verification of data. You can use it  for  two  pur‐
       poses:

              ·  calculating a digest of selected file from hard disc;

              ·  comparing  selected file with content of first track on optical disc; The second
                 option also utilizes digest tool (e.g. md5sum), but may not work  correctly,  so
                 it is marked in cdw as "Experimental".

       cdw  has  Configuration  window,  accessed  by selecting "Configuration" item in left-hand
       menu. The window allows you to set up some options for tools used  by  cdw,  and  for  cdw
       itself. Configuration window has following tabs (you can access them using keys F2-F6):

              ·  Log and misc - contains options related to log file in which cdw logs its opera‐
                 tions, and to some aspects of behavior of cdw.

              ·  Tools - options in this tab allow you to select tools from your operating system
                 that  will  be  used  to  create  ISO9660 images, burn data to disc etc. You can
                 safely leave "Configure tools manually" checkbox unmarked.

              ·  Audio - contains options related to ripping audio CDs.

              ·  Hardware - probably will be visited only once, when you use cdw  for  the  first
                 time  or when you change your hardware configuration.  Here you can set paths to
                 your devices used by cdw.

              ·  UDF - this tab contains only text information about  requirements  for  creating
                 UDF  file  system.  There  are no options available in this tab. All options for
                 creating UDF file system can be accessed through UDF image wizard.

                 All options available in Configuration window are described below, in CONFIGURA‐
                 TION section.

       You  have  to use F9 or F10 key to save any changes made in Configuration window and close
       the window. To close Configuration window without saving changes use  Escape  key.  Values
       from "Tools" tab are saved only temporarily, they aren't stored in permanent configuration
       file.

       You can control cdw using application's menu (visible on left side of  screen),  or  using
       hotkeys.

   Main menu items
              ·  Add  files:  Select files and directories that you want to write to optical disc
                 or to ISO image on your hard disc. Press Space to select a  file  or  directory,
                 use Up and Down arrows to move on the list, press Enter key to change directory,
                 press Escape key to close file selection dialog.

              ·  Delete files: List of selected files is displayed  on  the  right  side  of  the
                 screen.  If  you  want to delete files from the list, select this button and use
                 Delete key to delete a file, Up and Down arrows to move on  the  list  or  press
                 Escape to end deleting.

              ·  Write  files  to disc: write selected files to optical disc that is currently in
                 drive. You will be warned if there is no disc in drive, the drive is  not  ready
                 or the disc is mounted.

              ·  Create  image:  write  selected  files  to ISO9660 or UDF image file. The target
                 image file can be selected in file picker window.

              ·  Write image to disc: write ISO image file to optical disc. The  ISO  image  file
                 can be selected in file picker window. You will be warned if there is no disc in
                 drive, the drive is not ready or the disc is mounted.

              ·  Read disc: Copy content of your single session data CD/DVD or audio CD to  files
                 on  hard  drive. In case of data CD/DVD discs cdw will create correct image file
                 on your hard disc, but only for first track on disc, rest  of  tracks  won't  be
                 read  (cdw  can't  read them correctly). In case of audio CDs cdw will copy each
                 audio track to separate raw audio file. You will have to recode  the  raw  audio
                 track files in order to play them in your media player.

              ·  Erase disc: Erase data (partially or fully) from rewritable disc (CD-RW, DVD-RW,
                 DVD+RW) that is currently in drive. You can select  mode  of  erasing  CD-RW  or
                 DVD-RW: fast or full. You can't select mode of erasing of DVD+RW.

              ·  Verify  data:  Calculate digest of file selected from hard drive, or compare any
                 file with first track of optical disc.

              ·  Configuration: Selecting this menu option will display cdw Configuration window,
                 where you can set various options.

              ·  About: this option displays window with short information about cdw.

              ·  Quit: close cdw and return to your command line.

   Keys (hotkeys, available in main cdw window)
              ·  F1, H, ? - Show this help: Display help window with list of hotkeys.

              ·  F9/F10 - Show license of this program: display cdw license.

              ·  C - Show Configuration window

              ·  D  -  Show  information  about  disc:  cdw  will  run  external tool to get some
                 meta-information about optical disc in your drive and display  this  information
                 in text window.

              ·  E  -  Eject  drive  tray: open your optical drive tray (will close tray if it is
                 already open).

              ·  F - Write selected files to disc: Write  currently  selected  files  to  optical
                 disc.

              ·  G  -  Read  content  of  CD:  Copy content of your audio or data CD to your hard
                 drive.

              ·  I - Write ISO image to disc: Write ISO image to optical disc.

              ·  L - Show log of last operation: view file with record of latest operations.

              ·  R - Refresh information about disc: cdw will  run  external  tool  to  get  some
                 meta-information about optical disc in your drive and will display short summary
                 in lower-left part of main cdw window.

              ·  V - Verify data: calculate digest of selected file (and of first track on  opti‐
                 cal disc).

              ·  Q - Quit: close cdw and return to your command line

CONFIGURATION
       This  section  describes  Configuration  module  available  via  "Configuration" button in
       left-hand menu in main cdw window. You have to press F9/F10  in  Configuration  window  to
       save changes made in the window and exit or press Escape key to close configuration window
       without saving changes. Please note that changes made in "Tools" tab are  saved  only  for
       current session with cdw.

              ·  Log and misc (first tab):

                 ·  Log  file  path - path to file, in which cdw writes its messages and messages
                    from external tools (mkisofs, cdrecord, growisofs, dvd+rw-mediainfo, xorriso,
                    mkudffs, etc.). Specifying this path is obligatory.

                 ·  Show  log  after actions - show the content of log file after some of actions
                    performed by cdw.

                 ·  Volume size - size of ISO/UDF filesystem that you want to create. Most  often
                    it is equal to capacity of optical disc that you want burn data to. This is a
                    dropdown, from which you can select one  of  preselected  values,  or  select
                    "Custom size". Value of custom size can be entered below the dropdown.

                 ·  Custom  size  -  size of ISO/UDF filesystem that you want to create.  This is
                    the place where you can enter nonstandard value of target ISO/UDF  filesystem
                    size.

                 ·  a checkbox for following symbolic links in selected files. Read the text next
                    to the checkbox for more information.

                 ·  a checkbox modifying style of navigation in file system browser windows. When
                    this  field  checked, you can use Left/Right keys on keyboard to move up/down
                    in hierarchy of directories in file system. This  option  work  similarly  to
                    Midnight Commander's "Lynx-like motion" option.

              ·  Tools (second tab):

                 First  thing  that you will see in this tab "Configure tools manually" checkbox,
                 it is unchecked by default, and this is safe default value.  In this  state  cdw
                 select  tools  itself. If you check this checkbox you will be presented with six
                 dropdowns: first three allow you to select tools or families of  tools  intended
                 for specific tasks.  Remaining dropdowns allow you to select paths to some tools
                 used by cdw for some tasks. All these dropdowns are explained below.

                 ·  "Tool for creating stand alone ISO9660 file" - here you select a tool used to
                    create ISO9660 file saved on hard disc (stand-alone file).  Depending on con‐
                    figuration of your software, here you can find mkisofs (genisoimage) and xor‐
                    riso.

                 ·  "Tools  for handling CDs" dropdown - here you select a tool that will be used
                    to burn data to CD/CD-RW discs and to erase CD-RW discs.  Depending  on  con‐
                    figuration of your software, here you can find cdrecord (wodim) and xorriso.

                 ·  "Tools  for  handling  DVDs"  dropdown -  here you select a tool that will be
                    used to burn data to DVD discs and to erase  DVD+/-RW  discs.   Depending  on
                    configuration  of  your  software,  here  you can find dvd+rw-tools, cdrecord
                    (wodim) and xorriso. Note that xorriso can be used only for DVD+/-R discs.

                 ·  "Path to mkisofs" dropdown - here you can select one  of  implementations  of
                    mkisofs installed in your system (e.g. if you have mkisofs and genisoimage).

                 ·  "Path  to  cdrecord" dropdown - here you can select one of implementations of
                    cdrecord installed in your system (e.g. if you have cdrecord and wodim).

                 ·  "Digest tool" - here you can  select  a  tool  used  for  calculating  digest
                    (checksum)  - the tool is used e.g. during verification of burning of ISO9660
                    file to optical disc. md5sum tool is the default one, if found.

                    Remember that these settings are saved only for  your  current  session  with
                    cdw, and that the settings will be discarded when you close cdw.

              ·  Audio (third tab):

                 ·  Audio  output  dir  -  path to directory, into which cdw will write raw audio
                    tracks, ripped from audio CD.

              ·  Hardware (fourth tab):

                 ·  "cdw should use this drive" dropdown - option allowing you to select  one  of
                    detected drives, or to use path to drive entered manually ("custom path");

                 ·  "Custom  path  to  drive" - field in which you can specify your own path to a
                    device, to be used when cdw can't autodetect all drives in your computer.

                 ·  SCSI device - parameters describing your SCSI hardware, in following  format:
                    scsibus,target,lun  (for a SCSI-emulated IDE CDRW: 0,0,0). It is used only by
                    cdrecord and you should enter appropriate value only if cdrecord has problems
                    with  device  path in form of '/dev/xxx'. Otherwise this field should be left
                    empty. This option hasn't been tested, so use it at your own risk.

              ·  UDF (fifth tab): This tab contains only basic information about what is required
                 by cdw to create UDF file systems. Options related to UDF file system are avail‐
                 able in UDF image wizard.

                 Notice that cdw doesn't specify (almost) any  default  options  for  mkudffs  or
                 rsync.   User  has  to select himself the best set of options for these programs
                 that suit his needs.

                 "-a" option for rsync is a good starting point. If your version  of  rsync  sup‐
                 ports  "--info=progress2"  and you enter this option in rsync options field, cdw
                 will show a better progress indication.

OPTIONS
       Command line options are following:

       -h, -- help
              displays information about invoking cdw and its options

       -v, --version
              displays cdw version and copyright information

       --enable-dvd+rp-dl
              enables very basic support for DVD+R DL discs

       --escdelay=X
              modifies delay time between pressing Escape key in cdw window and reaction  to  the
              key

FILES
              ·  cdw.conf: Configuration file, usually stored in $HOME/.cdw/ directory.

              ·  cdw.log: Log file, by default stored in $HOME/.cdw/ directory. Path to this file
                 can be modified in Configuration window ("Log and misc" tab >  'Log  file  path'
                 field).

              ·  cdw.colors: File with definition of color schemes used to customize interface of
                 cdw.  File is stored in $HOME/.cdw/ directory.

              ·  temporary files: cdw uses some small temporary files to store  various  informa‐
                 tion.  They  are usually created in /tmp directory. If everything goes well they
                 are very short-lived and are removed as soon as they are no longer needed.

ENVIRONMENT
              ·  HOME - cdw assumes that this variable exists and is set to valid user directory.
                 If not, then user is asked to select some other 'base' directory.

              ·  PATH  -  cdw reads this environment variable to find directories with executable
                 files. cdw will search in these directories for some tools.

DIAGNOSTICS
       cdw uses log file ($HOME/.cdw/cdw.log by default) to store information about actions  per‐
       formed.  You can access this file using your file manager, or pressing 'L' key in cdw win‐
       dow. Read this file for any hints if you experience any problems.

BUGS
       There are still some bugs related to memory management.

       cdw is developed and tested almost exclusively on x64  GNU/Linux,  running  cdw  on  other
       platforms  may  produce  some platform-specific bugs. Currently I can't verify them in any
       way.

       cdw still fails to detect some problems reported by wodim/cdrecord mkisofs/genisoimage  or
       growisofs,  although  things  have improved in latest few versions. Support for xorriso is
       brand new, so there is a lot error messages printed by xorriso, that cdw  is  unaware  of,
       and will happily ignore them. It is recommended to read log file after every operation.

       Copying  data  CDs  to  ISO  image may finish with message "Problems occurred when copying
       tracks". This does not have to mean that output ISO file is corrupted: this be a  sign  of
       some  low-level  problems when reading from optical disc.  Similar message displayed after
       ripping audio CD may mean that one or more track files created on your hard disc  is  cor‐
       rupted and it is caused by invalid audio CD.

       If  you notice any other bugs please let me know. You can do this either using bug tracker
       on cdw project site (http://sourceforge.net/projects/cdw/) or by  sending  me  an  e-mail:
       acerion@wp.pl.

AUTHOR
       cdw  is  currently  developed by Kamil Ignacak (acerion@wp.pl). First developer of cdw was
       Varkonyi Balazs (http://sourceforge.net/users/vbali/).

COPYRIGHT
       Copyright (C) 2002 Varkonyi Balazs, Copyright (C) 2007 - 2016 Kamil Ignacak.  This program
       is free software; you can redistribute it and/or modify it under the terms of the GNU Gen‐
       eral Public License as published by the Free Software Foundation; either version 2 of  the
       License,      or      (at      your      option)      any      later      version.     See
       http://www.gnu.org/licenses/old-licenses/gpl-2.0.html for details.

SEE ALSO
       cdrecord(1),  mkisofs(8),  growisofs(1),  dvd+rw-mediainfo(1),  dvd+rw-format(1),  cdrdao,
       mybashburn,   k3b(1),   gnomebaker(1),   xorriso(1),  libisoburn1,  libburn4,  mkudffs(1),
       rsync(1), libcdio.

0.8.1                                      2 April 2016                                    cdw(1)
