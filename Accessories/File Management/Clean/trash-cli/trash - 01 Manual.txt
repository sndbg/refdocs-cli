TRASH(1)                             General Commands Manual                             TRASH(1)

NAME
       trash - Command line trash utility.

SYNOPSIS
       trash [arguments] ...

DESCRIPTION
       Trash-cli  package  provides  a command line interface trashcan utility compliant with the
       FreeDesktop.org Trash Specification.  It remembers the name, original path, deletion date,
       and permissions of each trashed file.

ARGUMENTS
       Names of files or directory to move in the trashcan.

EXAMPLES
       $ cd /home/andrea/
       $ touch foo bar
       $ trash foo bar

BUGS
       Report bugs to http://code.google.com/p/trash-cli/issues

AUTHORS
       Trash  was  written  by Andrea Francia <andreafrancia@users.sourceforge.net> and Einar Orn
       Olason <eoo@hi.is>.  This manual page was written by  Steve  Stalcup  <vorian@ubuntu.com>.
       Changes made by Massimo Cavalleri <submax@tiscalinet.it>.

SEE ALSO
       trash-list(1),  trash-restore(1), trash-empty(1), and the FreeDesktop.org Trash Specifica‐
       tion at http://www.ramendik.ru/docs/trashspec.html.

       Both are released under the GNU General Public License, version 2 or later.

                                                                                         TRASH(1)
