Usage:  anacron [-s] [-f] [-n] [-d] [-q] [-t anacrontab] [-S spooldir] [job] ...
        anacron [-S spooldir] -u [job] ...
        anacron [-V|-h]
        anacron -T [-t anacrontab]

 -s  Serialize execution of jobs
 -f  Force execution of jobs, even before their time
 -n  Run jobs with no delay, implies -s
 -d  Don't fork to the background
 -q  Suppress stderr messages, only applicable with -d
 -u  Update the timestamps without actually running anything
 -t  Use this anacrontab
 -V  Print version information
 -h  Print this message
 -T  Test an anacrontab
 -S  Select a different spool directory

See the manpage for more details.

