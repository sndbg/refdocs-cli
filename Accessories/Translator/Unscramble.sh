#!/bin/bash
#
# Purpose: To unscramble given letters into words
# Example usage: Unscarble.sh olw
# Output:
#    2 word(s) found
#    owl
#    low
#
# Source: https://www.networkworld.com/article/3605508/factorials-and-unscrambling-words-with-bash-on-linux.html

if [ $# == 0 ]; then
    echo -n "scrambled string> "
    read string
else
    string=$1
fi

function mixup {
    if [ "${#1}" == 1 ]; then
        word="${2}${1}"
        grep ^$word$ /usr/share/dict/words &> /dev/null
        if [ $? == 0 ]; then
            if [[ ! " ${words[@]} " =~ "$word" ]]; then  # add word if new
                    words[$n]=$word
                ((++n))
            fi
        fi
    else
        for i in $(seq 0 $((${#1}-1)) ); do
            pre="${2}${1:$i:1}"
            pc1="${1:0:$i}"
            pc2="${1:$((i+1))}"
            pc="${pc1}${pc2}"
            mixup "$pc" "$pre"
        done
    fi
}

mixup $string
echo ${#words[@]} "word(s) found"
for n in ${words[@]}; do echo $n; done
