#!/usr/bin/env bash

#The following prints a list, to which you can choose an item from that
#list and then in theory would play a video using mpv, but you could of
#course tweak this a bit to do whatever you wanted. This is just a
#simple example. Fzy is searchable.

declare -A video

video[Title of video 1]="https://urltovideo01"
video[Title of video 2]="https://urltovideo02"
video[Title of video 3]="https://urltovideo03"
video[Title of video 4]="https://urltovideo04"
video[Title of video 5]="https://urltovideo05"

title=$(printf '%s\n' "${!video[@]}" | fzy)

#If you want the videos to be sorted alphabetically, the use...
#title=$(printf '%s\n' "${!video[@]}" | sort | fzy)

if [ "$title" ]; then
    url=$(printf '%s\n' "${video["${title}"]}")
    mpv $url
else
    echo "Program terminated" && exit 0
fi
