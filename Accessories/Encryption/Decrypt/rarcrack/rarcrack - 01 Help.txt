RarCrack! 0.2 by David Zoltan Kedves (kedazo@gmail.com)

Usage:   rarcrack encrypted_archive.ext [--threads NUM] [--type rar|zip|7z]

Options: --help: show this screen.
         --type: you can specify the archive program, this needed when
                 the program couldn't detect the proper file type
         --threads: you can specify how many threads
                    will be run, maximum 12 (default: 2)

Info:    This program supports only RAR, ZIP and 7Z encrypted archives.
         RarCrack! usually detects the archive type.

