#####################
# Accessories Index #
#####################

aconnect - ALSA sequencer connection manager
aircrack-ng - a 802.11 WEP / WPA-PSK key cracker
anacron - runs commands periodically
ar - create, modify, and extract from archives
aseqdump - show the events received at an ALSA sequencer port
at, batch, atq, atrm - queue, examine or delete jobs for later execution
awk - pattern scanning and processing language
base64 - base64 encode/decode data and print to standard output
bashmount - tool to mount and unmount removable media
bc - an arbitrary precision calculator language
bcd, ppt, morse - reformat input as punch cards, paper tape or morse code
binclock - prints time in binary format
bleachbit - removes unnecessary files
bsdgames - collection of classic textual unix games
bzcat - decompresses files to stdout
bzip2, bunzip2 - a block-sorting file compressor
bzip2recover - recovers data from damaged bzip2 files
caesar - decrypt caesar ciphers
cacademo, cacafire - libcaca's demonstration applications
cdw - front-end for cdrecord, mkisofs, growisofs, mkudffs and other tools
chronos - an ncurses stopwatch/timer
cmatrix - simulates the display from "The Matrix"
cp - copy files and directories
crypt, mcrypt, mdecrypt - encrypt or decrypt files
cron - daemon to execute scheduled commands (Vixie Cron)
csplit - split a file into sections determined by context lines
cups - a standards-based, open source printing system
cut - remove sections from each line of files
date - print or set the system date and time
dd - convert and copy a file
detox - clean up filenames
du - estimate file space usage
efax - send/receive faxes with Class 1, 2 or 2.0 fax modem
expr - evaluate expressions
factor - factor numbers
file - determine file type
fzf - a command-line fuzzy finder
fzy - a fuzzy text selector menu for the terminal
gammu - does some neat things with your cellular phone or modem
gitfm - GNU Interactive Tools File Manager
gitkeys - GNU Interactive Tools - Display key sequence utility
gitmount - GNU Interactive Tools - auto-mount script
gitunpack - GNU Interactive Tools - Unified archive unpacking
gitwipe - an utility for wiping files
gpg - OpenPGP encryption and signing tool
gphotofs - filesystem to mount digital cameras
gpm - a cut and paste utility and mouse server for virtual consoles
grep, egrep, fgrep, rgrep - print lines matching a pattern
grive2 - open source Linux client for Google Drive
gzip, gunzip, zcat - compress or expand files
hashcat - World's fastest and most advanced password recovery utility
hubic-backup - hubiC backup command line tool for linux & OSX
jdupes - finds and performs actions upon duplicate files
kpcli - a command line interface to KeePass database files
locate - find files by name
lp - print files
lpq - spool queue examination program
lpr - off line print
lprm - remove jobs from the line printer spooling queue
ls - list directory contents
lsof - list open files
makepasswd - generate and/or encrypt passwords
mapscii - a Braille & ASCII world map renderer for your console
maxima - a system for the manipulation of symbolic and numerical expressions
mc - GNU Midnight Commander is a directory browser/file manager for 
     Unix-like operating systems.
mmv - move/copy/append/link multiple files by wildcard patterns
mount - mount a filesystem
mv - move (rename) files
number - convert Arabic numerals to English
octave - a high-level interactive language for numerical computations
openssl - OpenSSL command line tool
outguess - universal steganographic tool
p7zip - wrapper on 7-Zip file archiver with high compression ratio
parted - a partition manipulation program
paste - merge lines of files
pig - eformatray inputway asway Igpay Atinlay
pilot-link - a suite of tools for communicating with Palm handhelds
primes - generate primes
pwgen - generate pronounceable passwords
python - an interpreted, interactive, object-oriented programming language
rain - animated raindrops display
random, srandom, initstate, setstate - random number generator
ranger - visual file manager
rar - archive files with compression
rarcrack - password cracker for rar archives
rename - renames multiple files
rfcdiff - compare RFCs or Internet Drafts or any other text files
rm - remove files or directories
rot13 - a cipher in which each letter is pushed forward by 13 places
sane - Scanner Access Now Easy: API for accessing scanners
scanimage - scan an image
sed - stream editor for filtering and transforming text
shred - overwrite a file to hide its contents, and optionally delete it
shuf - outputs randomly shuffled line-by-line file content or list of files/folders
stat - display file or file system status
stegbreak - launches brute-force dictionary attacks on JPG image
stegdetect - finds image files with steganographic content
steghide - a steganography program
stegsnow - whitespace steganography program
synclient - commandline utility to query and modify Synaptics driver options
tar - an archiving utility
taskwarrior (task) - a command line todo manager
termgraph - a python command-line tool which draws basic graphs in the terminal
termsaver - a simple text-based terminal screensaver
trash-cli (trash) - command line trash utility
trans - Command-line translator using Google Translate, Bing Translator, 
        Yandex.Translate, DeepL Translator, etc.
tree - list contents of directories in a tree-like format
tty-clock - a terminal digital clock
umount - unmount file systems
unar - extract archive file contents
units - unit conversion and calculation program
unp - a shell frontend for uncompressing/unpacking tools
unrar - extract files from rar archives
Unscramble.sh - use this BASH script to unscramble letters into words
unsort - reorder file contents semi-randomly
unzip - list, test and extract compressed files in a ZIP archive
vdir - list directory contents
veracrypt - create and mount VeraCrypt encrypted volumes
vlock - Virtual Console lock program
wdiff - display word differences between text files
when - a minimalistic personal calendar program
worms - animate worms on a display terminal
wtf - translates acronyms for you
wyrd - a text-based front-end to remind(1), a sophisticated calendar and 
       alarm program
xmodmap - utility for modifying keymaps and pointer button mappings in X
xsetwacom - commandline utility to query and modify wacom driver settings
zip - package and compress (archive) files
