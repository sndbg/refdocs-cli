#################
# pkill - Basic #
#################

Example:

    pkill -f tmux
    
Kills all tmux related processes. This is helpful for when you don't feel
like opening htop, filtering with F4, and then selecting every processes
with space bar.
