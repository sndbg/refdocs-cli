Usage:
  xfce4-terminal [OPTION...]

General Options:
  -h, --help; -V, --version; --disable-server; --color-table;
  --default-display=display; --default-working-directory=directory

Window or Tab Separators:
  --tab; --window

Tab Options:
  -x, --execute; -e, --command=command; -T, --title=title;
  --working-directory=directory; -H, --hold

Window Options:
  --display=display; --geometry=geometry; --role=role; --drop-down;
  --startup-id=string; -I, --icon=icon; --fullscreen; --maximize;
  --show-menubar, --hide-menubar; --show-borders, --hide-borders;
  --show-toolbar, --hide-toolbar; --show-scrollbar, --hide-scrollbar;
  --font=font; --zoom=zoom

See the xfce4-terminal man page for full explanation of the options above.

