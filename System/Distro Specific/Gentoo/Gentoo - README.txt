###################
# Gentoo - README #
###################

Part of the way into adding my command-line notes, I realized that most
people that would even care about this are probably using systems like
Gentoo or Arch instead of distros like Ubuntu or Debian. Out of all of
them, Gentoo is the hardest and most time-consuming to get installed and
setup correctly.
