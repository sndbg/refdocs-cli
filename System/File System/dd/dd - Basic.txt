##############
# dd - Basic #
##############

I use dd to unpack .iso or .img files to a USB stick:

    dd if="/path/to/file.img" of="/dev/sdb"
    
To know where your USB device (the 'of' part) is, use:

    fdisk -l
    
You may have to run these as sudo.

You can monitor the progress of a 'dd' job by (in another terminal):

    dd if="/path/to/file.img" of="/dev/sdb" status=progress
