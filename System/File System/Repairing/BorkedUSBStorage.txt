Sometimes a USB storage device gets "borked" and will not mount without
giving you an error. You try to repair it with 'fsck', but things take 
forever and you may not care that much as to what is on the storage 
device and you just want to wipe the damn thing so you get to a point in
which you can use it again. If this scenario sounds familiar, this is 
what you do in the command-line:

1. Plug-in the "borked" USB storage device.

2. Use 'fdisk -l' to list devices and figure out which is the USB 
   storage device.
   
3. Run the following but replace "sdx" based on what you found in Step 1,
   as well as the "4G" part with the size of your USB storage device:

    sudo parted /dev/sdx --align opt mklabel msdos 0 4G

...This will wipe the USB but not create any partitions.

4. You should now be able to use 'cfdisk' to create a partition. The 
   reason this tool was not suggested before-hand is because you can
   "bork" a USB storage device to the point 'cfdisk', or even GParted,
   cannot see it.

------------------------------------------------------------------------

There was another instance in which I somehow "borked" a 32GB USB 
flashdrive. I could not create a partition table the normal way. What I 
_think_ fixed it was the following...

    sudo dd if=/dev/zero of=/dev/sdc status=progress
    
    Let this run until you get smilar output below and then use CTRL+c
    to cancel (larger USB storage devices may need more time):
    
    111690240 bytes (112 MB, 107 MiB) copied, 254 s, 440 kB/s
    
Then, run the following...

    sudo dd if=/dev/sdc bs=512 count=1 | hexdump -C
    
You will get similar output like so:

00000000  20 02 00 00 00 00 00 00  00 00 00 20 00 00 00 00  | .......... ....|
00000010  00 00 22 00 00 00 00 00  00 02 00 20 00 00 00 00  |.."........ ....|
00000020  00 01 00 00 00 00 02 00  04 00 04 00 00 00 00 00  |................|
00000030  00 00 00 00 00 00 00 00  00 00 00 00 20 00 00 10  |............ ...|
00000040  00 00 00 00 00 00 00 00  02 00 00 00 01 a0 00 00  |................|
00000050  00 02 00 00 00 00 00 00  00 00 00 00 00 84 00 40  |...............@|
00000060  00 00 00 20 00 00 00 00  00 08 00 00 00 00 00 00  |... ............|
00000070  00 00 80 01 00 00 00 10  00 00 08 04 00 00 80 00  |................|
00000080  00 00 00 00 00 00 01 00  00 00 42 00 00 60 00 00  |..........B..`..|
00000090  00 00 00 00 04 00 00 00  00 00 00 00 00 00 00 00  |................|
000000a0  10 01 00 00 00 00 02 00  80 10 00 10 01 01 00 00  |................|
000000b0  00 00 00 00 20 00 00 40  00 00 00 20 00 00 02 00  |.... ..@... ....|
000000c0  00 80 00 00 00 00 02 00  00 04 80 00 00 00 00 00  |................|
1+0 records in
1+0 records out
000000d0  00 00 80 80 00 00 00 00  00 00 00 00 00 00 00 00  |................|
512 bytes copied, 0.00276572 s, 185 kB/s000000e0  00 10 00 00 00 00 00 00  02 00 00 00 00 00 00 00  |................|

000000f0  00 00 00 00 00 00 00 00  00 80 00 00 01 00 00 00  |................|
00000100  00 00 00 00 00 40 20 00  00 20 00 00 00 00 02 00  |.....@ .. ......|
00000110  00 50 04 00 40 00 00 00  00 40 00 80 00 02 00 00  |.P..@....@......|
00000120  00 00 81 01 00 22 04 00  00 00 00 00 00 00 00 00  |....."..........|
00000130  20 00 00 90 00 00 00 00  00 00 00 00 00 00 00 00  | ...............|
00000140  00 00 01 80 00 00 00 40  00 20 00 00 00 00 04 00  |.......@. ......|
00000150  00 01 08 00 00 00 00 04  00 00 00 40 02 00 40 00  |...........@..@.|
00000160  00 00 00 00 00 08 00 00  20 00 00 00 00 01 00 00  |........ .......|
00000170  00 02 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000180  00 00 00 18 00 00 00 00  00 00 00 00 00 00 00 10  |................|
00000190  00 01 00 00 00 00 00 00  00 00 10 00 00 00 00 01  |................|
000001a0  10 00 00 80 00 00 00 00  20 04 00 00 00 00 00 00  |........ .......|
000001b0  08 20 00 00 00 04 00 00  00 00 00 00 00 00 00 10  |. ..............|
000001c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001d0  00 00 01 00 00 00 00 00  00 23 00 00 00 00 00 00  |.........#......|
000001e0  00 00 00 00 00 00 00 00  00 00 00 80 00 00 00 00  |................|
000001f0  00 00 04 00 00 00 00 16  01 00 80 00 00 10 00 00  |................|
00000200

Then, run the following...

    sudo dd if=/dev/zero bs=512 count=1 of=/dev/sdc status=progress
    
Followed by...
    
    sudo dd if=/dev/sdc bs=512 count=1 | hexdump -C 
    
You will then get similar output like so:
    
00000000  07 18 60 a0 01 33 2e b5  d0 07 18 02 09 14 82 58  |..`..3.........X|
00000010  38 18 13 1a 89 00 25 96  12 06 b2 50 63 79 20 62  |8.....%....Pcy b|
00000020  4b 08 17 06 22 b2 c4 70  11 e7 23 0a 04 04 21 42  |K..."..p..#...!B|
00000030  18 d1 88 5c 70 b9 a0 72  08 05 96 9b 0c eb 18 07  |...\p..r........|
00000040  48 94 20 47 58 02 32 64  b2 0c 95 40 93 00 30 8d  |H. GX.2d...@..0.|
00000050  50 e0 04 c3 20 55 02 ae  b0 00 72 84 25 9a 51 13  |P... U....r.%.Q.|
00000060  12 44 03 76 01 a0 00 40  02 94 81 8e 20 68 48 02  |.D.v...@.... hH.|
00000070  a0 04 72 e9 66 42 0a 50  14 19 6a 00 20 42 98 08  |..r.fB.P..j. B..|
1+0 records in
1+0 records out
00000080  62 94 0f 08 00 1c 80 08  61 02 a8 46 3c 12 40 40  |b.......a..F<.@@|
00000090  80 26 2c 10 81 09 08 07  5a ee b0 4c 12 24 e5 b3  |.&,.....Z..L.$..|
512 bytes copied, 0.106801 s, 4.8 kB/s000000a0  08 16 82 14 62 a2 c0 10  00 70 36 44 50 42 b4 3d  |....b....p6DPB.=|

000000b0  20 51 84 44 51 20 02 82  00 f0 15 00 ea 01 84 60  | Q.DQ .........`|
000000c0  65 68 2b 04 12 04 60 33  62 04 2c 47 01 e8 01 89  |eh+...`3b.,G....|
000000d0  80 01 91 d2 2c 5b 14 10  69 03 22 60 e9 80 4e a2  |....,[..i."`..N.|
000000e0  80 4a 60 62 06 a6 82 e0  e1 82 00 41 82 20 50 f6  |.J`b.......A. P.|
000000f0  89 80 61 a0 20 38 11 40  81 41 a5 62 72 88 43 3d  |..a. 8.@.A.br.C=|
00000100  41 c1 84 5c 03 39 69 22  ce c9 10 54 87 32 08 20  |A..\.9i"...T.2. |
00000110  55 00 2e 00 10 01 28 07  55 c0 b9 11 a2 64 71 0d  |U.....(.U....dq.|
00000120  a5 01 0a 1b 26 03 52 87  41 28 21 16 99 60 0a 42  |....&.R.A(!..`.B|
00000130  04 00 30 00 70 85 c4 53  09 44 85 01 10 28 40 ae  |..0.p..S.D...(@.|
00000140  a4 30 40 94 21 a6 40 56  02 a2 b1 28 45 00 13 1d  |.0@.!.@V...(E...|
00000150  c5 89 95 94 ee 0e 8c 1c  05 00 3d 16 c0 0e 90 04  |..........=.....|
00000160  44 02 18 80 a2 90 01 8c  24 20 c6 46 5e 10 10 52  |D.......$ .F^..R|
00000170  48 80 23 12 90 55 a0 19  35 10 00 0a 0c 07 05 80  |H.#..U..5.......|
00000180  98 11 2b 0c 53 54 60 26  00 1a 00 02 02 e6 19 80  |..+.ST`&........|
00000190  b2 67 33 02 15 c3 22 12  14 05 81 45 96 52 00 14  |.g3..."....E.R..|
000001a0  d0 30 03 42 88 08 0e 91  20 40 79 45 c3 69 88 68  |.0.B.... @yE.i.h|
000001b0  06 00 b8 00 13 0e 30 48  17 14 82 0e e0 44 4a 0a  |......0H.....DJ.|
000001c0  73 6a 44 0c 0a 10 05 00  18 00 2a 80 26 20 14 08  |sjD.......*.& ..|
000001d0  01 4a 04 57 22 da 90 88  6a e8 a9 0b 2c 04 00 03  |.J.W"...j...,...|
000001e0  48 01 75 ec 47 00 62 80  45 80 33 30 10 d2 8f 0c  |H.u.G.b.E.30....|
000001f0  24 08 3b 44 33 70 45 80  92 0b 24 c3 50 12 31 2d  |$.;D3pE...$.P.1-|
00000200

In other words, from zeros to whatever the heck this is BUT you should
then be able to run 'GParted' (yeah... I know... Command-line howto...) 
or something like 'cfdisk' to create a partition table without any 
overlap issues, followed by creating a FAT32 partition as one normally 
would for a USB storage device, though I guess exFAT will be the normal 
sooner or later.

[Update to above on 2022/01/01]...

Never mind, the 'dd' stuff mentioned above did not help at all. I think
there is just something wrong with the USB stick itself. It did appear 
to work for a minute there, but then decided not to the next day, giving
the same issues as before. The lesson to learn here is to by name-brand 
USB storage devices and to not buy the "cheapo knock-offs" that come in 
bulk; if it is too good to be true, it probably is. I just figured that
in 2021/22, even if it is a "cheap" USB, the technology behind it would
be good enough to not really matter, but it does.
