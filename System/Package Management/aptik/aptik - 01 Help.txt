----------------------------------------------------------------------
Selected user: (All Users)
----------------------------------------------------------------------


Aptik Migration Utility v17.4 by Tony George (teejeetech@gmail.com)

Syntax: aptik [options]

Options:

Common:

  --backup-dir <dir>    Backup directory (defaults to current directory)
  --user <username>     Select username for listing config files
  --password <password> Specify password for encrypting and decrypting backups
  --[show-]desc         Show package description if available
  --yes                 Assume Yes for all prompts
  --h[elp]              Show all options

Software Sources (PPA):

  --list-ppa            List PPAs
  --backup-ppa          Backup list of PPAs
  --restore-ppa         Restore PPAs from file 'ppa.list'

Downloaded Packages:

  --backup-cache        Backup downloaded packages from APT cache
  --restore-cache       Restore downloaded packages to APT cache

Installed Software:

  --list-available      List available packages
  --list-installed      List installed packages
  --list-auto[matic]    List auto-installed packages
  --list-{manual|extra} List extra packages installed by user
  --list-default        List default packages for linux distribution
  --backup-packages     Backup list of manual and installed packages
  --restore-packages    Restore packages from file 'packages.list'

Users and Groups:

  --backup-users        Backup users and groups
  --restore-users       Restore users and groups

Application Settings:

  --list-configs        List config dirs in /home/<user>
  --backup-configs      Backup config files from /home/<user>
  --restore-configs     Restore config files to /home/<user>
  --size-limit <bytes>  Skip config dirs larger than specified size

Themes and Icons:

  --list-themes         List themes in /usr/share/themes
  --backup-themes       Backup themes from /usr/share/themes
  --restore-themes      Restore themes to /usr/share/themes

Filesystem Mounts:

  --backup-mounts       Backup /etc/fstab and /etc/crypttab entries
  --restore-mounts      Restore /etc/fstab and /etc/crypttab entries

Home Directory Data:

  --backup-home         Backup user-created data in user's home directory
  --restore-home        Restore user-created data in user's home directory

Scheduled Tasks:

  --backup-crontab         Backup user's scheduled tasks (crontab)
  --restore-crontab        Restore user's scheduled tasks (crontab)

All Items:

  --backup-all          Backup all items
  --restore-all         Restore all items


