############################
# xinit - Customize startx #
############################

Place inside your '~/.xinitrc' file:
-----------------------------------

    # Here Xfce is kept as default
    session=${1:-xfce}

    case $session in
        i3|i3wm           ) exec i3;;
        kde               ) exec startkde;;
        xfce|xfce4        ) exec startxfce4;;
        # No known session, try to run it as command
        *                 ) exec $1;;
    esac

This way you can choose between desktop environments more easily if you
need to 'startx.' You can also use this method to start applications on
a "need-to-use" bases only instead of launching a heavy desktop environment
to run a simple GUI program. Example:

    case ...
        Chromium          ) exec lwm & exec chromium;;
        ...
    esac

You'd run it as 'xinit session' or 'startx ~/.xinitrc session' where 
"session" is the item in one of the cases mentioned above.
