#################
# What is cc65? #
#################

cc65 was originally a C compiler for the Atari 8-bit machines written by 
John R. Dunning. However, you can use it to write games for:

- Apple ][
- Apple //e
- Atari 8-bit
- Atari 2600
- Atari 5200
- Atari Lynx Game Console
- Bit Corporation Gamate Console
- Commodore 128
- Commodore 16/116
- Commodore 510
- Commodore 64
- Commodore 610
- Commodore PET machines
- Commodore Plus/4
- Commodore VIC20
- Creativision Console
- NEC PC-Engine (TurboGrafx) Console
- NES (Nintendo Entertainment System)
- Ohio Scientific machines
- Oric Atmos
- Oric Telestrat
- Watara Supervision Console
