______________________________________________________________________________
| SECTION 1 PART A SUBPART 1 | Coderz Series Part 2 |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Coderz Series Part 2:
        Welcome back! Well this issue I'm going to go into how to create yet
 more effects for any possible demos you might be writing. Just some simple
 effects this issue, you need a break after last weeks indepth view into Sine
 waves!

The STATIC Effect:

        The STATIC effect is fairly convincing, although there is a little
 patterning which distinguishes it from a standard TVs effect. The standard
 TV static effect requires quite a lot of speed (unless you use a good
 randomising algorithm and fast graphics drawing) so using standard QBASIC
 commands will not do. This zine doesnt promote source that only works with
 the help of libraries so how can we do it fast, in QBASIC?

 Simple, we draw a random pattern of grey dots onto the screen once then we
 use some palette cycling to simulate the effect of the pixel color changing
 every time. If we do this fast enough it looks roughly like a standard
 static effect. So how exactly do we go about this.. Well here's the steps
 of the program:

  --> create 15 shades of grey from black to white

  --> draw a random pattern of these fifteen shades to the screen

  --> cycle the palette!  15 to 14   14 to 13   1 to 15  etc.....

 It's that simple, so simple the code doesnt need much explanation except
 the comments written in!

SCREEN 13

' Loop creates grey shades in colors 1 to 15

FOR c% = 1 TO 15
OUT &H3C8, c%
OUT &H3C9, (c% * 3) + 2
OUT &H3C9, (c% * 3) + 2
OUT &H3C9, (c% * 3) + 2
NEXT c%

' Draw random color (1-15) to each pixel on screen

FOR x% = 1 TO 320
FOR y% = 1 TO 200
c% = INT((15 - 1 + 1) * RND + 1)
PSET (x%, y%), c%
NEXT y%
NEXT x%
SLEEP

' Read in palette values and cycle round

DO
OUT &H3C7, 1
tr% = INP(&H3C9)
tg% = INP(&H3C9)
tb% = INP(&H3C9)
FOR t% = 2 TO 15
OUT &H3C7, t%
r% = INP(&H3C9)
g% = INP(&H3C9)
b% = INP(&H3C9)
OUT &H3C8, t% - 1
OUT &H3C9, r%
OUT &H3C9, g%
OUT &H3C9, b%
NEXT t%
OUT &H3C8, 15
OUT &H3C9, tr%
OUT &H3C9, tg%
OUT &H3C9, tb%

' Wait for vertical retrace so that no flickering occurs

WAIT &H3DA, 8
WAIT &H3DA, 8, 8
LOOP


WAV Playing:

 Sound effects and noises are a large part of demos, if you want to be able
 to  play  a  wave  sound  through your soundcard then we have a _fast_ WAV
 all here for you to use!
 It is in Section 1b1. :)


Please request what you would like to see in the coders section and we will
endeavour to find BASIC code to do it! We need your suggestions! =)

Cheers, (oh lameness...)









--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #7 from November 1996.