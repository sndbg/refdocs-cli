*** GRAPHICS IN QBASIC TUTORIAL (PART 1) *************************************
by "Hacker" aka Damian Nikodem (Hacker@alphalink.com.au) *********************


Intro:

I am assuming that you HAVE some experience in basic programing. If you dont
then dont bug me with a &H7FFF (32767) E-mail messages about what is a
integer.
Now in this tutorial I will show you how to load(and understand!) many image
formats, speed up nearly ALL of your graphics routines, and nearly any other
aspect of graphics in BASIC. Now onto the tutorial!

Palette:
The palette is a series of numbers where the values of the colors is kept.
Or in other words if you change the palette you can change the colors in a
image. So if you want to draw a green gradient in Vga mode 13h (screen 13)
you would have to change the palette because there isnt enough green's in the
existing palette.
Now you may be thinking that this is hard to do but in reality it is EXTREMELY
easy. In fact it can be done in just 4 commands:
' start CODE
OUT &H3C8, col 
OUT &H3C9, red
OUT &H3C9, green
OUT &H3C9, blue
' end CODE
Now, the value that should be inside `col' is the color that you want to
change, the value in `red' it the ammount of RED you want in the color you
want to change,`green' and `blue' are the same execpt that they are diffrent
colours. The color values CAN'T be over 63 or it will have some intresting
errors. This code can be cycled incrementing one or more values to produce
some cool effects (red flashing screen)or this can be used just once to load a
image with diffrent colors.
Now there is a way to save and read palette's quickly and it has (nearly)
become a standard ammong basic programers here is the sub routines to load and
save the .PAL files
' start CODE
SUB file2pal (file$)
OPEN file$ FOR BINARY AS #1
r$ = " "
g$ = " "
b$ = " "
FOR x% = 0 TO 255
GET #1, , r$: r% = ASC(r$)
GET #1, , g$: g% = ASC(g$)
GET #1, , b$: b% = ASC(b$)
CALL pal(x%, r%, g%, b%)
NEXT x%
END SUB

SUB getpal (Colour%, Red%, Green%, Blue%)
OUT &H3C7, Colour%
Red% = INP(&H3C9)
Green% = INP(&H3C9)
Blue% = INP(&H3C9)
END SUB

SUB pal (Colour%, Red%, Green%, Blue%)
OUT &H3C8, Colour%
OUT &H3C9, Red%
OUT &H3C9, Green%
OUT &H3C9, Blue%
END SUB

SUB pal2file (file$)
OPEN file$ FOR OUTPUT AS #1
FOR x% = 0 TO 255
CALL getpal(x%, r%, g%, b%)
PRINT #1, CHR$(r%); CHR$(g%); CHR$(b%);
NEXT x%
CLOSE #1
END SUB
'end CODE

Now it is simple to use these routines just add
pal2file ("palette.pal")
to your code to send a copy of the palette to a file called `palette.pal'
and to load a saved palette you just use
file2pal ("palette.pal")
to load palette.pal
now that you understand this i'll go onto a FAST routine

Fast Graphics:

Have you notaiced that PSET is very slow! well you can do the same job by
writing to memory execpt it is a LOT faster
here is a small bit of code that writes color 30 to a (10,10)
'start CODE
screen 13
def seg= &HA000       ' Tells the computer to point to video memory
x = 10                ' The X position of the pixel on the screen
y = 10                ' The Y position of the pixel on the screen
col = 30              ' The color of the pixel
poke (y*320) + x, col ' Puts the pixel on the screen
'end CODE
The math on the last line is quite simple. The screen is simple to understand
this is a small representation of the screen every box is one pixel and the
number inside that box is the memory address of the pixel
+------+------+------+------+------+------+...
|  01  |  02  |  02  |  04  |  05  |  06  |
+------+------+------+------+------+------+
| 321  |  322 |  323 | 324  |  325 |  326 |...
+------+------+------+------+------+------+
| 641  | 642  | 643  |  644 | 645  | 646  |...
+------+------+------+------+------+------+
.      .      .      .      .      .      .
.      .      .      .      .      .      .
.      .      .      .      .      .      .
because VGA mode 13h (screen 13) has 320 pixles on the horozontal axis we
multiply y by 320 to figure out which line to write and we add x to figure out
which column the pixel is!
In the next part I will teach you about the PCX format
(if alex is shocked how big that one is he will DIE :] when he sees the size
of the GIF tutorial [or .JPG if I have the time])

- Hacker (Hacker@alphalink.com.au)

Note: DO NOT E-MAIL ME ABOUT SPELLING MISTAKES OR ANY OTHER GRAMMER ERRORS









--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #10 from April 1998.  This issue was edited by Alex Warren.