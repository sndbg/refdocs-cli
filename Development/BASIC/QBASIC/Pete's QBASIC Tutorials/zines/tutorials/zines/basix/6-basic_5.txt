______________________________________________________________________________
| SECTION 1 PART B SUBPART 2 | Basic Tutorials |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 BASIC Tutorials - instalment 5
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 If you remember, last issue I gave you an assignment:

  Write a program that clears the screen and then asks for the year that you
  were born in. Then have it work out how old the person is and then display
  it like so:

  You were born in (year%) and that means that now you are (age%) old.

 Let's go through this and try and write a program that would work:

 CLS                                                    ' Clears the screen
 INPUT "What year were you born in?",year%              ' Asks for the year
 age% = 1996-year%
 PRINT "You were born in ";year%;" and that means that now you are ";age%;
       "years old"

 Of course, this wouldnt work all the time, you would also have to take the
 month into account. In this example we have placed an expression to age% on
 the 3rd line ' age% = 1996 - year% ' but you can also place expressions
 direct into PRINT commands. For example:

 PRINT "4*5+99-6*2 = "; 4 * 5 + 99 - 6 * 2

 This would work perfectly fine. So you could shorten the assignment programs
 last line to:

 PRINT "Born in ";year%;" and now you are ";1996-year%;" years old"

 As you can see the expression has been placed actually inside the PRINT
 command. But in many cases this method should not be used. It can be used
 in 'one-off' situations like the above because it saves having to create
 another variable, but if the expression will be printed several times then
 theres two reasons why you should use a variable first:

        o  It saves a bit of typing
        o  It saves time in execution


---

 This  is  the  last instalment.  These  tutorials were designed for the most
 novice of programmers. They have dealt with *very* simple topics very slowly
 in  a  very  basic way.  The tutorials were not designed to teach people how
 to  write  demos  or  games,  they  were  designed  to teach the most novice
 programmer  the  absolute  basics (no pun intended) before they possibly buy
 a book. Most QBasic books probably rush past this sort of stuff in the first
 few pages, but now beginner programmers know for sure what to do.









--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #6 from August 1996.