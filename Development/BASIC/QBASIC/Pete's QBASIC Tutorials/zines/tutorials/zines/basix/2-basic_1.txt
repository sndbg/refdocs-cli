-------------------------------------------------------------------------------
- SECTION ONE PART B PART I - (Basic tutorial 1/5) ----------------------------
-------------------------------------------------------------------------------

 This is another new regular feature to appear in the fanzine. A set of 5 
 tutorials is being written at this moment and one by one they'll be in
 the fanzine so if you don't know any BASIC then here's your chance to learn!

 Hi all!
 I havn't got much room in this fanzine to do this so I better start straight
 away.

 I'm going to assume that all of you budding programmers have QBasic because
 it makes a good start. All of the stuff I teach should be compatable with
 PowerBasic so I think you should be ok.

 PRINTING TO THE SCREEN
 ======================

 In BASIC we use a command called PRINT to display text and variables (more
 on them later) to the screen. It's simple to use. To display text you can
 use:

 PRINT "hello,world"

 If you want to display numerals or numeric expressions you can use:

 PRINT 2+4+1-5          '(NOTE you would get the number 2)

 You can display alphanumeric stuff like so:

 PRINT "ABC123Can't you see?"

 That should all be easy stuff to grasp. Oh, if you don't know how to use the
 QBasic environment then please read the manual because I'm teaching BASIC 
 and not how to use menus and the like. Sorry! ;-)

 VARIABLES
 =========

 Simply, a variable is a memory store. You can store a number in a variable or
 you can store a line of text. Learn from example:

 a$ = "hello"

 This would put the text 'hello' into a memory cell called a$
 Or we could go:

 a% = 1000

 This would put the number 1000 into the a% box.

 a% = 1000 * 2
 print a%

 This is more complicated. a% equals 1000*2 in this case and so that makes
 2000. Then the next line prints the contents of the variable a% to the screen,
 which in this case is 2000. See?

 I'm sorry about the shortness of this article but I have just been told that
 the fanzine is a bit too long already so I've been ordered to stop. I'll
 make up for the shortness next fanzine. Sorry. 

 See yo!   DH


--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #2 from December 1995.