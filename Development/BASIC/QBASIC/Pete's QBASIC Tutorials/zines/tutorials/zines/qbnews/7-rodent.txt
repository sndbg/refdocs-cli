     ----------------------------------------------------------------------
        W h o   y a   g o n n a   c a l l ?   C A L L   I N T E R R U P T
     ----------------------------------------------------------------------

     Working with the Rodent by Daniel R. Berry
     
        The Rodent (mouse) is one of the easiest input devices for computer 
     users to adapt to.  It allows great flexibility for entering data, 
     moving around screens and using menus.  Over the last few years the 
     Rodent has truly found a home with computer users.   This leads you to
     wonder why new programs enter the market without mouse interfaces.
     Probably the most common reason for this oversight is the Rodent
     intimidates programmers.  The pesky little critter isn't the easiest
     user interface to program for.  Yet,  adding a mouse interface to your
     programs is easier than most think.
     
        The mouse functions operate through a device driver such as
     MOUSE.SYS or MOUSE.COM provided with most MS-DOS compatible rodent
     species.  This driver ties itself to interrupt 33H allowing the
     programmer direct accessed to the mouse functions.  Several programmer
     references document this interrupt and its functions detailing the
     features and complexities of the mouse driver.  Over the last few
     years I have played around with mouse interfaces and have finally
     developed a set of routines that allow me to add a mouse interface to
     any QuickBASIC program I write.  The routines provide access to most
     of the mouse functions and they aren't that hard to use.
     
     To use these routines in your program you must use the following 
     statement at the beginning of your program:
     
          COMMON SHARED /Mouse/ MouseChk%, MouseStat%
          (This statement is provided in the MOUSE.BI file.)
     
     The routines use these variables to track the following:
     
          MouseChk% indicates the availability of an active mouse driver 
     and the status of these routines (1=Enabled / 0=Disabled).  
     
          MouseStat% shows the status of the mouse cursor or pointer 
     (1=On / 0=Off).
     
        Mouse is the heart of the mouse utilities.  It provides access to 
     interrupt 33h using INTERRUPT provided in the QB.QLB.  Almost all the
     other mouse routines call Mouse to complete their functions.
     
          Usage:  CALL Mouse (M1%, M2%, M3%, M4%)
     
          M1 - M4 are the system registers AX - DX respectively.
     
        MouseCheck is the second most important of these routines.  
     MouseCheck can determine mouse driver availability; enable the mouse
     driver; or disable the mouse driver.  This routine must be called at
     the beginning of your program if you want to use the mouse.
     
     
     The QBNews                                                     Page 27
     Volume  2, Number  2                                      May 31, 1991

        MouseChk updates MouseChk% and MouseStat% as applicable.  You
     should note that when the mouse driver is installed the mouse cursor
     is set to off.
     
          Usage:  CALL MouseCheck (MFlag%)
     
          MFlag% = If set to -1 the mouse will be installed, if 
     available (if not already done so).  If set to -2 the mouse 
     routines will be disabled.  Any other value will cause this routine 
     to determine if the mouse has been installed.  A returned value of 
     1 indicates that the mouse driver is installed, 0 indicates that 
     the mouse driver has not been installed or is disabled.  
     
        MouseCRT will determine or set the mouse video display page.  This 
     is useful when your programs use multiple display pages.
     
          Usage:  CALL MouseCRT(CRT%, Flag%)
     
          The video display page is determined or set by using the CRT% 
     variable.  Flag% is used to toggle 0=set/1=determine.
     
        MouseInformation provides specific information for the Rodent used 
     by the system.  This routine provides the mouse driver software 
     version and mouse type.  
     
          Usage:  Call MouseInformation(MouseVer$, MouseType%,
                  MouseType$)
     
          MouseVer$ is the current mouse driver version.
     
          MouseType% and MouseType$ represent the following:
     
          1 - Bus Mouse       2 - Serial Mouse    3 - InPort Mouse
          4 - PS/2 Mouse      5 - HP Mouse
     
        MouseLeft determines the position of the mouse at the last left 
     button depression.  Also determines the number of depressions of the
     left button since this routine was last called.  Position is returned
     in screen pixels.
     
          Usage:  CALL MouseLeft (LCount%, MouseX%, MouseY%)
     
          LCount% = The number of left button depressions since the 
     routine was last called.
     
        MouseLeftRC functions the same as MouseLeft except position is 
     determined by screen row and column.
     
          Usage:  CALL MouseLeftRC (LCount%, MRow%, MCol%)
     
        MouseLightPen will enable (1) or disable (0) mouse light pen 
     emulation mode.  Calls to the Basic PEN function return the last pen
     down location.  Pen down is set when both buttons are depressed.
     
     
     The QBNews                                                     Page 28
     Volume  2, Number  2                                      May 31, 1991

          Usage:  CALL MouseLightPen (Status%)
     
        MouseLimits will set the mouse limits restricting movement to a 
     specific area.  Limits are set using screen pixels.
     
          Usage:  CALL MouseLimits(MouseX1%, MouseY1%, MouseX2%, 
     MouseY2%)
     
          MouseX1/Y1% = Upper left corner horizontal/vertical (X/Y) 
     position.
     
          MouseX2/Y2% = Lower right corner horizontal/vertical (X/Y) 
     position.
     
        MouseLimitsRC functions the same as MouseLimits except the limits 
     are determined by screen row and column.
     
          Usage:  CALL MouseLimitsRC(MRow1%, MCol1%, MRow2%, MCol2%)
     
          MRow1%/MCol1% = Upper left corner row/column limit.
     
          MRow2%/MCol2% = Lower right corner row/column limit.
     
        MouseLocate locates the mouse cursor at a specified X and Y 
     coordinate.  Coordinates are set in screen pixels.
     
          Usage:  CALL MouseLocate(MouseX%, MouseY%)
     
        MouseLocateRC functions the same as MouseLocate except coordinates 
     are set at specified row and column. 
     
          Usage:  CALL MouseLocateRC(MRow%, MCol%)
     
        MouseMotion returns the horizontal and vertical mouse motion 
     counters.  Motion counts are in 1/200 inch increments. 
     
          Usage:  CALL MouseMotion(Horz%, Vert%)
     
          Horz% = Horizontal motion counter value (positive = right 
     motion / negative = left motion).
     
          Vert% = Vertical motion counter value (positive = downward 
     motion / negative = upward motion).
     
        MouseOn / MouseOFF turns the mouse cursor on/off as desired.  These 
     functions use MouseStat% to determine the status of the cursor and 
     take action only when needed.  This check is used to prevent the 
     internal cursor flag from being modified unnecessarily.
     
          Usage:  CALL MouseOn / MouseOff
     
        MouseRelease returns the position of the mouse after the button 
     requested was last released.  Position is returned in screen pixels.
     
     
     The QBNews                                                     Page 29
     Volume  2, Number  2                                      May 31, 1991

          Usage:  CALL MouseRelease(Button%, Count%, MouseX%, MouseY%)
     
          Button% = The button to be checked (0 = left / 1=right).
     
          Count% = The number of button releases since the last call to 
     this routine.
     
        MouseReset resets the mouse driver to the default values and
     updates MouseStat%.
     
          Mouse Position:  Screen Center     Mouse Cursor:  Off
          Light Pen Emulation:  On           CRT Page Number:  0
     
          Usage:  CALL MouseReset
     
        MouseRight determines the position of the mouse at the last right 
     button depression.  Also determines the number of depression of the 
     right button since this routine was last called.  Position is returned
     in screen pixels.
     
          Usage:  CALL MouseRight(RCount%, MouseX%, MouseY%)
     
        MouseRightRC functions the same as MouseRight except position is 
     determined by screen row and column.
     
          Usage:  CALL MouseRightRC (RCount%, Row%, Col%)
     
        MouseSensitivity enables programmers to determine or modify mouse 
     sensitivity to better suit their needs.  Horizontal and vertical 
     movement are defined in mickeys.  One mickey equals 1/200-inch of 
     mouse travel.  Mickeys range from 1 to 32,767 (horizontal default is 8
     and vertical default is 16).  Doublespeed sets threshold for doubling
     mouse cursor movement (default 64 mickeys/second).
     
          Usage:  CALL MouseSensitivity(GetSet%, HMickey%, VMickey%,
                  DoubleSpeed%)
     
          GetSet% is used to toggle 0 = Get / 1 = Set.
     
        MouseStatus determines Mouse Status to include position and buttons 
     currently depressed.  Position is returned in screen pixels.
     
          Usage:  CALL MouseStatus(Left%, Right%, MouseX%, MouseY%)
     
          Left% = If returned as 1 then the left button is depressed.
     
          Right% = If returned as 1 then the right button is depressed.
     
        MouseStatusRC functions the same as MouseStatus except position is 
     returned by screen row and column.
     
          Usage:  CALL MouseStatusRC(Left%, Right%, MRow%, MCol%)
     
        RODENT.BAS provides a demonstration of these routines and designed 
     
     The QBNews                                                     Page 30
     Volume  2, Number  2                                      May 31, 1991

     to give you a general idea of how the mouse interface works.  If your
     rodent is having a problem working with these routines read your users
     manual to ensure that you have MS Mouse emulation active.
     
        Well, that's about it.  I have included a few support routines from 
     my library to give RODENT.BAS some flair and the *.BIT routines are 
     needed by MOUSE.MOU to work with some of the bits returned in the 
     system registers.  You have everything you need to get that pesky 
     rodent to work so why not put that $50 - $90 toy to some use and 
     program!  
     
        Please feel free to write.  I am open to comments, suggestions and 
     will even answer a question or two.  Complaints are received only on
     Friday, must be in triplicate (no carbons please), must be signed by
     your mother, and be accompanied by a personal check for $200.00.
     (Laugh - my wife did!)
     
     [EDITOR'S NOTE]
     All source code for this article is contained in RODENT.ZIP.
     
     **********************************************************************
          Daniel Berry is a member of the U.S. Air Force.  Off-duty he 
     writes a utility library for QuickBASIC.  Daniel may can be reached 
     at 3110-C South General McMullen, San Antonio, TX 78226.
     **********************************************************************