                  QB ON ACID
ISSUE #4                            December 21st, 1999
_________________________________________________________________

                            PlatForm Engines 101
                                      
   To tell ya the truth, platform engines are really easy to code. A lot
   easier than what i thought it'd be. To start out, you use a regular
   pixel engine. Yep, that's right, a regular pixel engine. If you don't
   know what that is, it's basically a tile engine that can scroll using
   pixels. And if you don't know what a tile engine is, then you really
   don't need to read this, not to be mean or anything, but learn to
   create a tile engine, then I'll be glad to help with a pixel engine.
   First off, to create the pixel engine, start off with a tile engine,
   and add tile offsets. So lets say you have map offsets; Xoffset,
   Yoffset Those are our map locations for the upper lefthand tile. The
   tile engine would set the screen up like so:
   
 _ _ _ _ _ _ _ _ _
|a|_|_|_|_|_|_|_|_|  If Xoffset and Yoffset = 1 then the block "a" is
|_|_|_|_|_|_|_|_|_|  where xoffset and yoffset are. Easy huh?
|_|_|_|_|_|_|_|_|_|
|_|_|_|_|_|_|_|_|_|  Adding Tile offsets are just as easy. Lets say
|_|_|_|_|_|_|_|_|_|  tiles are 16x16, your basic tile. When scrolling
|_|_|_|_|_|_|_|_|_|  you add to the offset until it reaches 16 segments
|_|_|_|_|_|_|_|_|_|  then set it back to 0, and adding 1 to the map offset.

   Sounds simple enough right? In laymans terms:
   
  TileX = 0

  SUB ScrollRight
    TileX = TileX + 1        'TileX is the tile offset for the X coords.
    IF TileX > 15 THEN       'starting TileX at 0 and adding 16 gives you 15
      TileX = 0              'put TileX back on 0 (default for next tile)
      Xoffset = Xoffset + 1  'move the map X coords over 1
    END IF
  END SUB

   I told you it was easy. Adding detection is pretty simple too, just
   check the Tile offsets against the map coords, and see if the tile is
   walkable.
   
   Yea, i know, this isn't REAL pixel by pixel, but i never said it was.
   =)
   
   Also, you may want to use a PUT routine that has clipping
   capabilities, which lets you place a tile half on the screen, most
   libraries allow this, i think, i don't use any except Qb13h. And i
   KNOW it does clipping. Ok, on to the platform.
   
   Platforms are the same, except you don't need to scroll up and down,
   or, at least you really don't need to. So maps can be longer. The way
   i do maps:
   
DIM SHARED map%(1, 200, 15)

   This allows me to have a 2 layer map system that is 200 tiles long and
   15 tiles high. Like i said, you don't NEED to scroll up and down,
   though you can if ya want.
   
   It's the same as what i just went thru above, using a pixel engine,
   just only able to scroll left and right. Only difference in them is
   the graphics that you use, instead of an RPG tileset, you'd use a side
   tile set. With cliffs and holes and things inside it.
   
   When placing a character into the engine, you just supply an X and Y
   coord for him to start. Usually on a ground level. While scrolling and
   finding where the character is, you check for map location, collision
   detection is better terms. I'm sure you are all familiar with it. You
   check the X and Y locations against the FIRST layer on the map, if
   it's a tile you can't walk on, the player can't move. If he CAN walk
   on it, then of course he can go across it. Also you would need to
   check above and below the player to check for holes and cliffs.
   
IF map%(0, PlayerX, PlayerY + 1) < 0 THEN 'check below player for
        MakePlayerFall                          'nonwalkable tile
END IF

   It's simple to implement, well, simple to me, i know, and I'm tryin to
   explain it the best i can.
   
   One thing i forgot to mention. You will need to add character tile
   offsets that tell if the character is between tiles. It works the same
   way as the tile offsets. When scrolling right, you'd add 1 to the
   PlayerXTileOffset.
   
   After PlayerXTileOffset > 15 then you'd add 1 to the PlayerX coord,
   moving the player along. This is a MUCH easier way to compare map
   locations with the player coords. It saves lots of time and trouble
   later.
   
   When making the player jump, you want to use tile detection again,
   checking 1 tile above the player while he is moving up, then when he
   starts coming down, you check below him, all the while checking the 1
   tile in front of the direction he is facing. Hope that wasn't too
   confusing. (Any questions just email me =)
   
   When showing the map, you just show the first layer, then show the
   player, then place the top map layer over top, so you get a "walk
   behind" effect. This effect adds really great looks to a good engine.
   While placing the second layer, to speed things up, make sure that you
   have a tile that is not placed. So that you won't need to go thru the
   whole loop of placing tiles.
   
   Say tile #17 isn't goin to get placed for the second layer, I would do
   something like:
   
DIM SHARED tiles(200, 5)
DIM SHARED map%(1, 200, 15)

SUB ShowMap
x = 0: y = 0
FOR b = Yoffset TO Yoffset + 13        'places 13 (16x16) rows on the screen
  FOR a = Xoffset TO xoffset + 20      'places 20 (16x16) cols on the screen
    Puttile (x, y), tiles(0, map%(0, a, b))  'routine for placing tiles
    x = x + 16                         'moves location over 16 pixels
  NEXT a
  x = 0: y = y + 16                    'starts new row
NEXT b

PutPlayer

x = 0: y = 0
FOR b = Yoffset TO Yoffset + 13        'places 13 (16x16) rows on the screen
  FOR a = Xoffset TO xoffset + 20      'places 20 (16x16) cols on the screen
    IF map%(1, a, b) <> 17 THEN        '17 being the tile not going to get
                                        'placed
      Puttile (x, y), tiles(0, map%(1, a, b))  'routine for placing tiles
    END IF
    x = x + 16                         'moves location over 16 pixels
  NEXT a
  x = 0: y = y + 16                    'starts new row
NEXT b

END SUB

   That's usually what my map show sub looks like. Placing the first
   layer of the map (map%(0, x, y)) and then placing the player
   (PutPlayer), then placing the second layer of the map (map%(1, x ,
   y)). Simple right? I hope so. Hopefully this article made sense, if
   not, email me and i will try to explain it a bit more in more detail.
   Thanks for your time!
   
                                [sshot.gif]
                                      
                                By Necrolyte