----------------
The Boolean Hell
----------------

	6 XOR 4 = 2. Yes my friend, it seems the madness of boolean "logic"
is at work again. What is that you ask, well, that is a histroy lesson.
But I can sum it up in one word "sex", and Mr. Boolean's lack of getting.

	You see, Boolean was this math dude from long ago. And he saw all
these other math dudes, making "cool" math discoveries, which begot fame, 
which begot, glory, which begot sex. So Boolean thought, "I want sex, I 
better think of something cool." So young Boolean studied and searched, but 
alas, was as dumb as a post, and couldn't come up with any new formula or
theorm that other math dudes thought was cool. So, he says, "Well hell, I'll
just make up my own math damn it!" So Boolean came but with TRUTH TABLES.
OH NO, I SLEPT IN PRE-CAL, I DIDN'T LEARN THEM! (well it sucks to be me!)
Basicly, Truth tables compared 2 things. here is an example:

A= TRUE
B= FALSE

A and B = B, because both A and B aren't true, the result is false
A or B = A, because Either A is True or B is True, the result is true
B or B =B, because neither is true, the result is false

	Get it. Well, Everyone thought it was cutting edge and neato, but
it had ABSOLUTLY NO use in his time. "oh cool, A and B = A, wow." That was
all you could do with it, break the ice at parties. Well, Boolean didn't care
because he was famous and was getting some every night until he died a very
happy man.

	When you get down to it, all your computer can do is compare stuff, 
and return the result of that compare. Adding is just complicated comparing 
(which I will show later), and all other math functions can be broken down 
into alot of adding. 

	So, what does all this do with Boolean logic. Well, thats easy.
Computers are electrical do-hickies, so they can only have 2 ways of 
expressing stuff. High current, Low current. Why not medium current? I don't
know, but I'm sure Intel could tell me. Any way. High and Low and like true
and false. Which is Boolean! Hundreds of years after he's dead, his invention
is useful.

	OK, so how does this work? Well, you have to make a system of numbers 
that can only have trues or falses. Thus binary was created. It goes like 
this:

13 in binary = 1101 because:

(2^3)(1) + (2^2)(1) + (2^1)(0) + (2^0)(1) = 13
(8)(1)   + (4)(1)   + (2)(0)   + (1)(1)   = 13
8        + 4        + 0        + 1        = 13

See, and 37 in binary = 100101 because:

(2^5)(1) + (2^4)(0) + (2^3)(0) + (2^2)(1) + (2^1)(0) + (2^0)(1) =37
(32)(1)  + (16)(0)  + (8)(0)   + (4)(1)   + (2)(0)   + (1)(1)   =37
32       + 0        + 0        + 4        + 0        +1         =37

	So, In Qbasic, AND, OR, NOT, XOR, IMP, and EQV are your Boolean tools 
Don't worry about NOT, IMP, and EQV, there aren't used alot. What Qbasic does 
when using these tools is convert the 2 numbers being compare into binary. 
For example lets compare 21 with 9. First, we convert them to binary.

21=10101
9 =01001               

Qbasic adds 0's to the front of the numbers until they are both the same 
amount of digits. In this case a single 0 was added to the front of 9's
binary form so that both numbers in binary have 5 digits

Next, Qbasic compares Bit by Bit (On/off by On/off) the numbers, depending
on which operator you use. I'm using OR in this example, which returns a
TRUE (1) if either bit is equal to 1, and a 0 if both bits equal 0.

21 OR 9 = 29 
-------
1  or 0  = 1    one of the them =1 so it is true
0  or 1  = 1    one of the them =1 so it is true
1  or 0  = 1    one of the them =1 so it is true
0  or 0  = 0    neither are =1 so it is false
1  or 1  = 1    one of the them =1 so it is true

11101 is binary for 29.

(2^4)(1) + (2^3)(1) + (2^2)(1) + (2^1)(0) + (2^0)(1) =29
(16)(1)  + (8)(1)   + (4)(1)   + (2)(0)   + (1)(1)   =29
16       + 8        + 4        + 0        +1         =29

x AND y : Both x and y must be true for the result to be true
x OR y  : Either x or y must be true for the result to be true
x XOR y : Either x or y must be true for the result to be true, but if
	  both are true, then the result is false

	Those are the only ones you really need to learn, but the truth 
tables of the other tools are in Qbasic's help file. Here is an example of 
XOR.

108 = 1101100
14  = 0001110

108 XOR 14 = 98
---------------
1       0  = 1
1       0  = 1
0       0  = 0
1       1  = 0
1       1  = 0
0       1  = 1
0       0  = 0

1100010 is binary for 98


--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #2.
This was written by Lord Acidus.