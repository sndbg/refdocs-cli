PRINT "(N)ew Game"
PRINT "(L)oad Game"
PRINT "(S)ave Game"
PRINT "(I)nstructions"
PRINT "(Q)uit"

DO
 a$ = INKEY$
 SELECT CASE UCASE$(a$)
  CASE "N"
   PRINT "You selected New Game!"
  CASE "L"
   PRINT "You selected Load Game!"
  CASE "S"
   PRINT "You selected Save Game!"
  CASE "I"
   PRINT "You selected Instructions!"
  CASE "Q"
   PRINT "You selected Quit!"
   PRINT "Have a nice day!"
   SYSTEM
 END SELECT
LOOP
