                          ......................   
                         /                      \   
                        /      Random in QB      \
                       /   By Meech Productions   \
                      /  Reply to mooch23@msn.com  \
                      \----------------------------/

If the text is flying off the screen go to Edit, then word wrap!

===============
 Introduction
===============

This tutorial is meant for begginers, those who possibly would like to use a command that makes certain numbers or variables to be chosen at random.  Making things randomly choosen in QuickBasic is very easy and there is plenty of stuff that you can use them with!

-Randomly Select a play to go first-
-Use Randomize statements for dice rolls-
-Create Simple guessing games and or lotteries-
-make certain colors and other things flash-
-use them in RPG games so random enemies are drawn at random times-
-Use them in RPG games so an attack doesn't inflict the same amount of damage each time
-Randomly choose a game from a compilation or song from a playlist-
-Make gimmicky looking objects-

And I am sure you can think of much more!

But how is this done? By using RANDOMIZE TIMER and RND. 

=========================
 RANDOMIZE TIMER and RND
=========================

At the top of the area where you are making variables random put the command RANDOMIZE TIMER. Well, two commands actually, RANDOM and TIMER. RANDOM resets a variable each time so the number isn't always the same. TIMER is supposedly a function that returns the number of seconds since midnight, but together they can execute random expressions. 

Next you will want to use this expression(s):

 
variable# = INT(RND * 15) + 1

variable# is the random number that statements will produce. INT stands for and integer, I believe. RND randomizes and multiplies 15 or whatever number you choose. (I chose 15 because their is a total of 15 colors, but will get to the color example in a moment) Then the plus 1 adds one so the range of numbers will be 1-15 instead of 0-14. If you would like the range of numbers to be 0-14, just take the "+1" out! Now let's check out the first example...

RANDOMIZE TIMER
variable# = INT(RND * 15) + 1
PRINT variable#

Now open QB and run this multiple times. This program will produce a completely random number each time! And this time it is real magic. No corny, false tricks are used such pulling a rabbit out of your ear or a card from under your sleeve.

I hope you understand that...It should be very simple, but to re-hash, RANDOMIZE TIMER resets it and RND makes the random number. That's basically all you need to know, but I will show you cool, cinchy programs you can make using RANDOMIZE TIMER and RND, just for chuckles and giggles.

Here's an example of a dice roll.

RANDOMIZE TIMER
variable# = INT(RND * 6) + 1
PRINT "You rolled a " variable#

Very similar to the above program. All you do is change the number to 6 instead of 15.

Let's see what you can do, but you using colors this time! Here's an example of how you could make decorative text for a person. 

INPUT "Name "; name$
RANDOMIZE TIMER
DO
color1# = INT(RND * 15) + 1
COLOR color#
LOCATE 1, 1 'This keeps it in the same place
Print "Happy Birthday "; name$ "!"
LOOP WHILE INKEY$ = ""

A pretty colorful program in only a few lines of text! See how powerful variables are in using random programs like this? The variable color1# is a random number, and you can use variables in just about any command thats syntax is [COMMAND] variable#. If  you would have liked, you could have made a variable for LOCATE row#, column# so the Happy Birthday Message would jump around the screen!

here's an example using graphics!

SCREEN 12
DO
RANDOMIZE TIMER
size# = INT(RND * 100) + 1
color1# = INT(RND * 15) + 1
CIRCLE (100, 100), size#, color1#
LOOP WHILE INKEY$ = ""

(You could use this to hypnotize your friends)

Another simple program, the circle size will be from 1 to 100, and it will have a random color too, and since there is a DO above RANDOMIZE TIMER and a LOOP at the end of the program QBasic will keep on drawing it. The CIRCLE is always plotted at 100, 100, therefore, it won't be bouncing around the screen.(If that would happen the screen would very quickly be filled with circles, unless if you put a CLS somewhere above the CIRCLE and somewhere below the DO.)


==========================================================
 Using IF....THEN statements to get more specific results
========================================================== 

But the above programs only give you a choice of numbers like 1-15 or 5-10, etc. But what happened if you wanted the numbers 1, 3, 7, 10, and 24? You can get more precise in QBasic, by making your progam more complex by using IF...THEN Statements.

For a start, lets say you wanted the fantastic circle above, but you wanted the program to CLS if the color1# = 0(black).

SCREEN 12
DO
RANDOMIZE TIMER
size# = INT(RND * 100) + 1
color1# = INT(RND * 16) 'Instead of 1-15, it will draw colors 0-15 now. 
IF color1# = 0 THEN CLS
CIRCLE (100, 100), size#, color1#
LOOP WHILE INKEY$ = ""

We only made a simple change in this tutorial! We changed the range of colors to 0-15 by removing the " + 1" and by changing the integer in the parentheses from 15 to 16. Then we made an easy if then statement- IF color1# = 0 THEN CLS!!!! Not much to it. But let's say you wanted the circle to be only three colors, and you were feeling patriotic, blue(Color 1), red(Color 4), and white(color 15).

SCREEN 12
DO
RANDOMIZE TIMER
size# = INT(RND * 100) + 1
culla# = INT(RND * 3) + 1
IF culla# = 1 THEN color1# = 1 'blue
IF culla# = 2 THEN color1# = 4 'red
IF culla# = 3 THEN color1# = 15 'white
CIRCLE (100, 100), size#, color1#
LOOP WHILE INKEY$ = ""

This program is a little more complex, but it shouldn't be that hard to understand. First we have to make/change the random color variable to a different one, I chose culla#. Then you have to change the value in the parentheses to how much colors you want. Three colors was the case here(red, white, and blue) and then make three IF...THEN statements. If culla# = 1 then color1# = 1. This will set color1# to blue!

Let's try another similar example-(But a more complex one this time!)

CLS
DO
RANDOMIZE TIMER
culla# = INT(RND * 2) + 1 
IF culla# = 1 THEN color1# = 2
IF culla# = 2 THEN color1# = 10
LOCATE 5, 1
COLOR color1#
PRINT " _______   _    _   ______"
PRINT "|__   __| | |  | | |  ____|                                   "
PRINT "   | |    |  __  | |  __|"
PRINT "   | |    | |  | | | |____"
PRINT "   |_|    |_|  |_| |______|"
PRINT "                  "
PRINT "                  "
PRINT " __  __              _______   _____    _____  __   __"
PRINT "|  \/  |     /\     |__   __| |  __ \  |_   _| \ \ / /"
PRINT "| \  / |    /  \       | |    | |__) |   | |    \ V /"
PRINT "| |\/| |   / /\ \      | |    |  _  /    | |     > <"
PRINT "| |  | |  / ____ \     | |    | | \ \   _| |_   / . \"
PRINT "|_|  |_| /_/    \_\    |_|    |_|  \_\ |_____| /_/ \_\"
row# = INT(RND * 25) + 1
column# = INT(RND * 80) + 1
number# = INT(RND * 10)
LOCATE row#, column#
PRINT number#
LOOP WHILE INKEY$ = ""


O_0 First I selected the two greens for colors...I made the logo LOCATEd at 5, 1... At the bottom the row# and column# are to random variables for LOCATE, so the a random number#(0-9) will be placed at a random place on the screen. 

But in the movie not just numbers are used! Oh no. They use random symbols, letters, and numbers. You may be thinking "Oh, there is no way I am going to create an if statement for each symbol on the keyboard! I've got a life.." Okay, that's great buddy, I'm sure you do, but I can guarantee it will only take 30 seconds more of your little time! Okay, first of all take out the the command "PRINT number#". Then go back to the line "number# = INT(RND * 10)"  Replace the 10 in the parentheses with 256. Then before the final line of the module put "PRINT CHR$(number#)" Now run the program. 

Cool isn't it? This will print letters and numbers, as well as special ASCII keys that aren't avaible on the keyboard. And it wasn't hard at all! Go to the help files then ASCII Character keyboard codes. These are the possible values that number# can be. A total of 255, so set the the value for number# in the parentheses to 256! (the character codes range from 0-255). Now, you may be wondering what CHR$ is. Open a new program and type "PRINT CHR$(27)"
This will print the letter "I". What CHR$ does is proccess an ASCII code.  Okay, now back to the matrix thingy. The number# in CHR$ will print a random value! So if number# = 82 it will print "R"! And that's all there is to it!

=========================
 Legal Notice/Conclusion
=========================

Well, that's all I will cover in this tutorial. Before was pretty complex, and if you understand that you should have no problem with figuring out ways of how to choose a random player to go first in a game, or how much damage an attack will inflict without causing the same amount each time. If you cannot figure that out, or this tutorial didnt help you to your needs, feel free to drop me an email. Constructive criticism and comments are also very much welcome. Send your emails to mooch23@msn.com! Thanks for reading!

Now, feel free to print a copy of this, make origami, or feed it to your goat! But I do take some pride in my work, so you definetly may not make any profits out of this, or claim it as your own! Not that you would want to... If you would like to distribute this or use it on your site or whatever, just drop me an email, and I'll be sure to let you! Thanks!

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
This tutorial is a copyright of Meech Productions written on June 12, 2004. 

 


 