;Hello!                                             10:27 december 3th 1998
;
;Welcome to this tiny tutorial about making your own home-made libraries
;using assembly. If you want to do anything with this document except reading
;it, you'll have to have Microsoft QuickBASIC v4.5 (incl. LIB.EXE and
;LINK.EXE) and something that resembles the Macro Assembler from Microsoft
;(MASM.EXE). I used the Turbo Assembler from Borland (TASM.EXE) in MASM-mode
;(not IDEAL).
;
;This document was NOT written to teach you assembly, but to show you how to
;make your own library. (Save this file as ADD.ASM)
;I think the best way to learn it is with an example and the comments, so here
;we go:
;
;
; <- these characters resemble the REM or ' statement

.model medium,basic     ;directive for the medium model
.code                   ;directive for code segment header
.286                    ;directive to allow 286 opcodes

public Addition         ;declares procedure Addition callable from QuickBASIC

;to use the function in QuickBASIC add this line to your program:
; DECLARE FUNCTION Addition% (firstvar%, secondvar%)

Addition        PROC    FAR     ;all procedures (SUBS/FUNCTIONS) must be
                                ;declared FAR, for the code segment could not
                                ;be the same as the one you're calling your
                                ;procedure from!

                push bp         ;save base pointer on stack
                mov bp,sp       ;use base pointer instead of stack pointer
                mov ax,[bp+6]   ;last variable (stack!)
                mov bx,[bp+8]   ;first variable (stack!)
                pop bp          ;restore base pointer from stack

                add ax,bx       ;add bx to ax without carry

                RET 4           ;4 is the number of BYTES (2*INTEGER!) you
                                ;used (got from the stack)

Addition        ENDP            ;if declared as FUNCTION, AX contains the
                                ;result
                END             ;directive to stop the assembler

;after turning this file into an object file (*.OBJ) you can add this to your
;library:
;
; :\> LIB ADD.LIB
;       - this doesn't exist yet, so answer [y]
;       - when the display says: "operations:" type   +ADD.OBJ
;       - press enter to skip the other fields.
;
;now you have your ADD.LIB, and you want you ADD.QLB, so you can actually use
;it: (though you need the .LIB file to make an .EXE file, to compile your
;program)
;
; :\> LINK /QU ADD.LIB
;       - press enter twice
;       - type:   BQLB45.LIB
;
;and now you have your QLB! :)
;
; Good luck with your own QLB!!!
;
;                                       Spirit and Source
;
