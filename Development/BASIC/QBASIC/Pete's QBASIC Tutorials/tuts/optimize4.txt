                                Optimiz4.Txt

			       by  Danny Gump
			danielgump@chainmailsales.com
		    http://chainmailsales.com/virtuasoft

        This text file is the fourth in a guide for use by QB/QBASIC
programmers to help optimize the program code for greater efficiency and/or
give a program a more professional look..  If you have any quetions, contact
VirtuaSoft at gump@gnt.net.  Address to Danny.

        (Note: This is not for the beginning programmer.  A strong
        background in QB/QBASIC programming is highly recommended.)

        4. DEBABELIZING
                a. True-color -> 8-bit color
                b. Lowering a picture's resolution
                c. Using a picture's palette in your programs
                d. Making a custom palette

4. DEBABELIZING

        a. True-color -> 8-bit color
                If you want to use scanned pictures or high quality artwork
        in your programs, you must first open the image in a paint program
        and save it as an 8-bit picture.  This is required because the screen
        with the most colors in QBASIC is 13H.  It only has 256 colors
        (8-bit) when most scanned images have 16,777,216 colors (24-bit,
        true-color).  Only then can you successfully open the image in QBASIC
        without any errors.

        b. Lowering the resolution
                Most (if not all) paint programs should have an option to
        resize a picture.  To get a picture to fit on screen 13H, you must
        be sure that it is 320*200 or less resolution, so use a paint
        program to resize the picture to a size that's appropriate to your
        program's needs.
                If you want to resize a picture that's already in QBASIC,
        load it onto the screen and use one of the following formulas:

        FOR x% = startx% TO endx%
           FOR y% = starty% TO endy%
              PSET (x%, y%), POINT(x% * Factorx%, y% * Factory%)
           NEXT
        NEXT

        or...

        DEF SEG = &HA000
        FOR x& = startx& TO endx&
           FOR y& = starty& TO endy&
              POKE x& + 320 * y&, PEEK(x& * Factorx& + 320 * (y&*Factory&))
           NEXT
        NEXT

                (Factorx and Factory are the factors by which the picture
        will be compressed in size.

        c. Using a picture's palette
                All 8-bit picture formats that are opened in a paint program
        include a palette.  I don't have all the details on how each format
        stores the palette (go to www.wotsit.com for that), but I can help
        you if you are using BMPs.  Try the loader I have on my web site at
        www.gnt.net/~gump under "Files->Utilities."  In it, there is an LONG
        INTEGER array, pal, that stores the picture's palette.  Simply save
        all 256 elements of the array (0-255) as a palette file, and you'll
        have your palette when you reload the file!

        d. Custom palettes
                Now you can load a picture in QBASIC, but what if each
        picture has a different palette?  This will surely scamble the
        colors if you try to load two or more pictures (except grayscales,
        which all have the same gradient palette) in the screen at a time.
        So how do you give all pictures the same palette?
                For this, you must have Adobe Photoshop or another paint
        program that allows for custom palettes.  What you do is you use the
        values in your palette x4 as the values for the custom palette
        because all image file formats save their palettes as values of 0-255
        and palettes for non-SuperVGA screens use 0-63.
                After making your custom palette, refer to "part a" of this
        tutorial, adding the step of using a custom palette while changing
        the number of colors to 256.

               ... This concludes the lesson ...
