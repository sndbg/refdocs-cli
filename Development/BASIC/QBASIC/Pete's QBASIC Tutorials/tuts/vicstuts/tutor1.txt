


                     Vic's QBasic Programming Tutorial

                           Basic Tutorial II




A. Introduction To Programming
        1. What's it all about?
        2. Why Qbasic?
        3. Should I even Try Programming?
B. Qbasic BASIC Commands



A. Introduction To Programming...
        1. What's It all About?

        Have you ever played a game and wondered, How did they Make this?
What Tools did they use and how did they learn all this?  I remember asking
that same question to myself before.

When you are at a doss prompt and you type in a program name and press Enter
you are giving the computer One command to do.  In a real program you are
doing the same thing only you would most likely have more than one command
to give.

        2. Why QBasic?

        When you think of Basic you usually think easy... Right?
QBasic is just that, but only when compared to other programming languages.
For the longest time the BASIC Computer Language has been the Begginers
programming language.  The only thing really easy about it is how it is set
up.  BASIC has easy to understand words and set up, but it still gets you in
the mindset that allmost every programming language has.  The only computer
programming language my Computer teacher knows is BASIC and look what he's
doing!

        3. Should I Even Try Programming?

        You can easily memorize every command in QBasic and still not know
how to program like the "Pros".  You have to know how to use the commands in
just the right way in order to do some things, and sometimes that can take a
lot of thinking.  Many effects you may want to use in your program may seem
immposible for you to think up, and often times they are.  Knowing this, why
would you even think to start?  For the longest time the only kind of games
I could write were text games where you enter a word and something happens.
I don't know how to explain it but one day things just clicked in my head and
I found out little tricks that you could do to manipulate and interact with
things on the screen.  I am sure that this will eventually happen to you.


NOW HERE WE GO!!


B. BASIC COMMANDS

The first command that almost any BASIC programmer learns is PRINT.
First I will give you 3 Examples of the command PRINT

-----------------------------------------------------------------------------
1. Print "Hello World!"
2. Print 5
3. print 5 + 3

1.
In the first one { Print "Hello World!" } you tell the computer to write
on the screen Hello World.  You probably ask What are the "" for?
In order to print WORDS you need the ""'s That is all you need to know
right now for that one... we'll get into it more complex later.

2.
In the second one { Print 5 } there are no "" around the 5 right? Then
how is it printed?  You can print numbers to the screen by either using
""'s around the five like { Print "5" } or you can leave them out and
get the same effect.

3.
In the third one { Print 5 + 3 } you are telling the computer to add
5 to 3 and then print the resaults on the screen.  If you used the command
{ Print "5 + 3" } with the ""'s the computer would print 5 + 3 and not
what they are when they are added to each other.
----------------------------------------------------------------------------

For right now you don't need to think about # 2 or 3 until later.

Imagine that you wanted to make a program that wrote on the screen
Vic Rules!!!  How would you do that?  Look at the first one and guess before
you look at the answer!

This is what that program would look like...

PRINT "Vic Rules!!!"

Thats it!
Try It!
open QBASIC and type
PRINT "Vic Rules!!!"
then hit run or F5 And BANG!  It prints it to the screen!

You gave the computer one command to do, and it did it...
Just like the dos prompt Example...
Memorize this command and how you use it!!
-----------------------------------------------------------------------------

Now lets enter a new Command into your repitoir...
CLS

The command CLS is short for  CLear the Screen
I am sure you can guess what this does...
If you have been writing your own programms with the print command you might
notice that what you wrote with your other programms is still on the screen
and you have to print underneath it...
CLS clears up this problem!

Here is a sample program...

-------------------------
CLS
Print "Hello World!"
-------------------------

Every time you run this program you can't see what you wrote in the old
program because before you print it the screen is cleared and it starts back
up at the top again.

I don't think you realy need any more information on this command

-------
THE END command

when you want to end the program then use the command END
enough said...
-------
-----------------------------------------------------------------------------

First Memorize the three previous commands then you can learn the
INPUT Command...

Whith the input command you Need to type 2 things...
1. INPUT
2. The Name of the input ( Handle )

The first is obvious but the second will take a little explaining...
The name of what you want to input is called an ARRAY...
ARRAY's Are one of the most important things you will learn in programming
An array can handle Numbers Or words.

A word can be defined like this
word$ or whateveryouwantocallit$
It has to have a $ at the end, Make sure you memorize this!!!!

A Number can be defined like this
Number  or  whateveryouwantocallit
this doesn't have to have a $ at the end.

Here is an example of how you would get input for a word and a number...
Word
----------
Input Name$
----------
Number
----------
Input Number
----------

When you run the program with the command
Input a$
and then type something in and push enter what you typed in will be stored in
the memory under the name a$.  When it is stored you can do many things with
it.  One could be to Print it to the screen.  This is easy.
Remember when I told you that you could print numbers without the ""'s?
Well, you can do the same with an array even if it doesn't contain a number.
To print a$ all you need to do is give the command...
Print a$       THATS IT!

Here is an exapmple program that gets the name of the person who is using it
and print it to the screen.

---
print "What is your name"
input name$
print "Hello..."
print name$
print "Thank you for using this program!"
---
one quick way to optimise thi program is to write it like this...
---
Input "What is your name ";name$
print "Hello ";name$
print "Thank you for using this program!"
---
instead of 5 lines I used 3 and you get the same effect!!!
If you look closely at this program you should be able to figure it out...

-----------------------------------------------------------------------------

Memorize the last one yet?
Here is the next important command...
Welcome to the IF command...
Every programming language that I have used so far has some form of the IF
command even if it is in the form of CMP JMP (ASM)...

The IF command has to be followed by the THEN command and lastly the END IF
command.
We'll start off with an example

INPUT NAME$
IF NAME$ = "Vic" THEN
PRINT "Hello Vic!"
END IF

EXPLANATION:

First you input your name...
after you do that the computer is told to look at what you inputed and 
compare it to "Vic".  If what you inputed is equall (=) to "Vic" Then
print "..." And then finally you put the end if to tell the computer to
stop doing that thing...

!************| IMPORTANT |*************!
One of the most common errors you will get will be from not Using the
END IF after your if...
!**************************************!

Here is another example.


If 1 = 5 then 
print "It does?"
end if

In this example 1 does not equal 5 so guess what it does...
It doesn't do anything actually...  It skips over until it gets to the 
END IF and continues on.  Nothing will be printed to the screen.

If 5 = 5 then
print "Thats right!"
end if

In this example 5 does equall 5 so guess what it does...
It does what its told to do and prints "Thats right!"

----------

You can write more than one command in an if statement...
Here is an example, guess what it does.

Input "Do you want me to print the numbers between 1 and 5" ; yesorno$
if yesorno$ = "yes" then
print "1"
print "2"
print "3"
print "4"
print "5"
END IF

As you have hopefully guessed, it ask you a question, if you reply yes,
then it prints 1 to 5 on the screen.

-----------

Another important command that is in allmost all programming languages 
is the FOR command.
The FOR command contains two parts, the FOR and the NEXT command

here is an example

FOR i = 1 to 10
print "Huh huh"
NEXT i

EXPLANATION:

The first line { FOR i = 1 to 10 } tells i to start at 1
and do what is below (until you hit the NEXT i ) 10 times.
If you havn't already guessed, the above program prints "Huh huh" 10 times.
This is an important Command to understand and make sure you have it down!
This type of LOOP is used a lot in game makeing...
(*(*(*(  It seems that a lot of programmers get in the habit of using   i 
         to use as a loop, but you don't always have to )*)*)*) 


I don't realy know how else to descibe what For does, I will give you an
example though.

-------

For i = 1 to 100
print i
next i

-------
in this example, it tells the computer to print what i equals 100 times,
every time the For runs one time to the NEXT i and back 1 is added to i.
when the command PRINT i is given, the number that i equals is printed 
to the screen.

-----------------------------------------------------------------------------
Here comes a big example that uses all of the commands from above...


CLS
PRINT "How many times would you like me to print VIC RULES"
input "0, 2, 10" ;number
if number = 0 then 
END
END IF

if number = 2 then  
cls
for i = 1 to 2
print "VIC RULES"
next
end
end if

if number = 10 then
cls
for i = 1 to 10
print "VIC RULES"
next
end
end if


----------------------------------------------------------------------------
First Understand the program and then you can learn more commands...


-----------------------------------------------------------------------------


The GOTO / GOSUB is another way to use a loop...

when you are programming you might want to leave a note where something is
so that you can go back to it.  In order to go back to that you can use 
the command GOTO.

there are two ways of leaving a note in a certain place.
1.  You can use a number Ex.  10
2.  you can use letters, as long as they have a : at the end
    Ex.  DUDE:

here is an example...

---

cls 
10 
print "Hello"
goto 10

---

In this example hello is printed forever...
to stop this press Control + Pause
or controll + C

Here is another example...

----
cls
input "pick a number between 1 and 4";number

if number = 1 then 
goto one
end if

if number = 2 then
goto two
end if

if number = 3 then
goto three
END IF

if number = 4 then
goto four
END IF



one:
print "you chose the number one"
end


two:
print "you chose the number two"
END


three:
print "you chose the number three"
END

four:
print "you chose the number four"
END
----------------------------------------------------------------------------
I hope you understand now, I can't think of any other way to describe it...
----------

The next command is used in almost all games It is the 
LOOP
Command...
The loop command has two parts, the first is the DO and then the LOOP
you absolutely need both of them...

do you remember the program...

---

cls 
10 
print "Hello"
goto 10

---
?

if you wanted to do the same thing with the loop command you would do it like
this


cls

DO
print "Hello"
LOOP

I think you understand this, now here is a way to add the LOOP UNTIL command.

DO
input number
loop until number = 1


This will loop until you input the number 1
The only other way to describe this better is to tell you to try this in
your own programms...

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

Thats it for this tutorial!
You have learned everything that you need to know to start making QBasic 
games.  In future tutorial I will tell you how to use this in more productive
ways. Thank you for spending time reading my crappilly spelled tutorial. And 
I realy hoped you learned something.





Vic Luce
1999
E-MAIl RadioHands@aol.com
