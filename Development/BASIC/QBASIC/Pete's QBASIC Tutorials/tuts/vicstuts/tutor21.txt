                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XXI!!

                           What Have I missed!!

                 Its about time I filled in the holes!!!

-----------------------------------------------------------------------------

Through the past tutorials, I have covered subjects that have nothing to
do with each other...

Since, I havn't gone in order, I have left a few gapes within the tutors...
And, quite frankly, That sucks...

I think it was my second or third tutorial that covered graphics...
And, in one of the examples I had a command like this...

screen 7,0,1,0
pcopy 1,0

This is like the third tutorial, and I am allready covering paging...

I think I told you that you shouldn't worry about that now, and to just
use it...

Well, Its Time that you learn it!!!


-----------------------------------
-0-0-0-0-
PAGING

page-ing
(I think that's how you spell it...)
(drop the E and ING right?, Don't E-mail me about that please...)

Ok, Here is the Skinny on what Pages are all about...


If you use PSET (x,y),clr
you put a pixel on the screen right?
right?  If you don't know that, you are in the wrong numbered tutor...

So, here is how you use PSET...

SCREEN 7

PSET (5,5),4


What that little example would do is set the screen to screen mode 7
and put a RED (4) at the coords x5,y5...


*Important Point*

But, what If I Want to draw a picture in the background, but I don't want to
show it on the monitor yet???

You know, If I wanted to get the picture All drawn and ready to go before
I put it on the screen...

*/Important Point/*



What would I do?

This, as you may have guessed, is where pages come in...

What you can do is, instead of drawing on the screen directly, you can
draw on a PAGE, that is hidden, that is, it is hidden until you want the
world to view it...

The cool thing about using a page, is that when you are ready to view the
page, It DOESN't have to draw the picture PIXEL by PIXEL...

It comes into view Immediatly...

So, there is allmost no wait when you want to see the next picture...
But, if you didn't do this with a page, and you erased the screen and started
to draw the picture Pixel by Pixel, the screen would FLASH...
Wich, in my opinion, Sucks like H*...

But, if you did it with a page, NO FLASH!!!  NO FLICKER!!!
SWEEEEEEEEEEEEEEET......

So, lets go back to the crypt, and pick out my old example...

'Example Start.....

SCREEN 7, 0, 1, 0
x = 100: y = 0
x2 = 120: y2 = 100
xadj = 1: yadj = 1
delay = 1
DO
press$ = INKEY$
LINE (0, 0)-(320, 200), 0, BF
PSET (x, y), 4
LINE (x2, y2)-(x2 + 10, y2 + 10), 1, BF
PCOPY 1, 0
IF y <= 20 THEN yadj = 1
IF y >= 180 THEN yadj = -1
IF x >= 300 THEN xadj = -1
IF x <= 20 THEN xadj = 1
x = x + xadj: y = y + yadj
IF x > x2 AND x < x2 + 10 AND y > y2 AND y < y2 + 10 THEN : PRINT "HIT": END
IF press$ = "w" THEN y2 = y2 - 1
IF press$ = "s" THEN y2 = y2 + 1
IF press$ = "a" THEN x2 = x2 - 1
IF press$ = "d" THEN x2 = x2 + 1
FOR i = 1 TO delay: NEXT
LOOP UNTIL press$ = "q"

'Example finished...

----

Look at the first line...

SCREEN 7, 0, 1, 0

What the furk does this do?

Well, look at the first part...


Screen 7...


Guess what?
This puts us into screen mode 7!!!

Now, what about the next little part?

0, 1, 0

This is tricky...
And confusing...
And, Quite frankly I'm not sure I know what it is...
But, here is what I really think is going on here...

The first 0, sets the main screen...

The 1, sets the "ACTIVE" Screen, or in other words, the page that you are
going to draw on, Notice, that You are not Drawing on the Main screen, so
you won't see the pixels when you start to draw on the screen...

And, The last 0,,,  Well, Thats anyones guess...  It might be important,
I don't know...

So, what we can tell from the above code, is...

1.) the main page, or the "Viewing Page" is labled by the number 0...
2.) The "Active" page, or the invisible page is labled by the number 1...
3.) We are going to start drawing on page 1, so, you won't see the pictures
    on sreen until we tell it to...

So, Now, How do we tell the computer to show the page?

Easy...

The drawing page is page 1 right?
The Visible page is page 0 right?

So, all we need to do, is copy page 1 to page 0, the visible page...
Now, How is that done...

Like this...

PCOPY 1,0

If you look close, it looks like pcopy could be the same as-
PAGE COPY...

And, since we are copying page 1, to page 0, we put a 1,0 after pcopy...


Its that simple...  Or, its that complex, however you want to look at it...

----

One very good tip to use when using pcopy, is to only use it when the
ENTIRE picture is ready to go...

Meaning, DON'T USE PCOPY WHILE YOU ARE DRAWING!!!
use it AFTER you have drawn the entire picture!!!

Good luck, and use it wisely...






---------------------------------------------------------------------------

Another Command that I have skimped on, is this one...

WAIT &H3DA, 8

What does that do?
---

Well, in a way, it works much like a page...
But, you call it right before you draw the picture onto the screen...

Why would you do that?

For example,
pretend you are writing a game in screen 13...
Well, screen 13 doesn't support pages...
So, your animations and movement will most likely flicker!!!
That sucks...

But, Think about this...

Your monitor is constantly blinking on and off...

What if you could catch the monitor when it was off, and clear the screen
and draw the picture then, before the monitor flickers back on...

If you draw while the monitor is off, when the monitor turns back on, it will
look as if you drew the picture using pcopy, or get and put...

And, if you do this enough, you will eliminate flicker!!!!
Think about it,,,
you can have 256 colors, and you don't have to worry about flicker!!!
SWEEEEEEEET

---

So, again, what does the command WAIT &H3DA, 8 do?

Well, quite simply, It WAIT's until the monitor flickers OFF...
When the monitor flickers off, you program quickly continues to draw on the
screen, and hopefully it draws all of its pixels before the monitor flickers
back on...

So, it would kind of go like this...



Wait until monitor flickers OFF
  * Ok, the monitor flickered off, lets continue... *
Draw draw draw
  * monitor flickers back on *
The picture is shown, and looks as if it was drawn quicker than it actually
was!!!


Not convinced?

Lets do a little example!!!


This is withough WAIT &hd8bla bla bla

'--- Start donut example...
DIM picture(1000), sprite(1000), spritesh(1000)

SCREEN 13
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
CIRCLE (10, 10), 4, 4
PAINT (10, 10), 0, 4
GET (0, 0)-(20, 20), sprite
LINE (0, 0)-(20, 20), 255, BF
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
CIRCLE (10, 10), 4, 4
PAINT (10, 10), 255, 4

GET (0, 0)-(20, 20), spritesh
CLS
FOR i = 1 TO 500
LINE ((RND * 320), (RND * 200))-((RND * 320), (RND * 200)), (RND * 255)
NEXT

x = 1: y = 1
xadj = 1: yadj = 1
delay = 10000
GET (x, y)-(x + 20, y + 20), picture

DO
press$ = INKEY$

PUT (x, y), spritesh, AND
PUT (x, y), sprite, OR

oldx = x
oldy = y

x = x + xadj
y = y + yadj


'***************** COMMENTED OUT!!! ****************
'WAIT &H3DA, 8
'***************** SUCKS WITHOUGHT IT!!! ***********


PUT (oldx, oldy), picture, PSET

GET (x, y)-(x + 20, y + 20), picture

'PUT (x, y), sprite, PSET

PUT (x, y), spritesh, AND
PUT (x, y), sprite, OR



IF y > 170 THEN yadj = -1
IF y < 10 THEN yadj = 1
IF x > 290 THEN xadj = -1
IF x < 10 THEN xadj = 1

FOR i = 1 TO delay: NEXT
                              
LOOP UNTIL press$ = CHR$(27)

'--- FINSISH DONUT example...



Now, Take the Comments off the 'WAIT &H3DA, 8
ie...

looks like

'WAIT &H3DA, 8

I want it to look like

WAIT &H3DA, 8

..................................

Wow, huh...  It is much much cleaner with WAIT &H3DA, 8 right?...

And you didn't believe me...

------------------------------------------------------------------
-0-0-0-



What else...?

I don't think I have talked about GET & PUT yet have I?

Ok, I'll cover it now...


First, you have to give the object you want a name...
For this, we will call it Dude...

Ok, now we need to know what part of the screen we want to capture...

For this, we can use a Block Filed square...
so, Draw what you need on the screen, and then, test out where you want to
capture...

so, if you do this command...

line (50,50)-(75,65),1,b

and if the box fits around the object, you know that is where you want
to get it from...

So, we have a name and coordinates...
Lets put it all together...





Ok, what do we do with the name?

Well, we have to make sure that the computer knows that we want to use the
GET command...

So, we have To DIM the array...
How much do we have to dim it?

In my opinion, as much as you want...
Unless you are very stingy with Memory, don't worry about it...
I'm sure there is some mathematical way to calculate how much memory you
need, But I don't care...
So, if you know the "Mathematical" way, DON'T e-mail me and tell me about
it!

So, lets dim our sprite...

DIM dude(1000)

1000 sounds good...

Know, how do we get the sprite?

Easy...
use the get command...

remember the old line command?

line (50,50)-(75,65),1,b

just change it a bit...

GET (50,50)-(75,65), dude


now, how do we put it back on the screen...

Well, that is a bit easier than geting the sprite...

Just use the put command...

So, pretend we wanted to put the sprite at 50,50...
I think you can guess how the command will look...

Just remember,

GET looks like the Line command

PUT looks like the PSET command

so, this is how it can be done...

PUT (50,50), dude

---
Thats it...

It doesn't get more boring than that...

-----------------------------------------------------------------

Ok...
Am I done here?

What else do you want to know?

Nothing...

OK...  Great...


----------------------------------------------------------------------------
****************************************************************************
----------------------------------------------------------------------------

   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------------- Untitled -------------------------------�+-+
�Thats it for this tutorial, If I didn't get into enough detail in the         
�explanations then just look at the source code and try to figure it out
�on your own.  All else fails E-Mail Me...  I want to make these tutorials     _
�as easy to understand as posible!!!                                           _
�                                                                              _
�My current E-Mail address is RADIOHANDS@AOL.com                               _
�                                                                              _
�If you are using this tutorial on your page, please leave the tutorial        _
�exactly as it is... please don't change anything, unless its spelling         _
�errors... Theres alot of them! I don't like using the backspace key...        _
�                                                                              _
�The original website that these were on is                                    _
�                                                                              _
�http://members.aol.com/radiohands/index.html                                  _
�                                                                              _
�Thank you                                                                     _
�Vic Luce                                                                      _
�Finished                                                                      _
�January, 5                                                                    _
�2001                                                                          _
�                                                                              
� ___________________________________________________________________________�
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00001:008


...
If you want to be notified when a new tutorial is out..
Send An E-mail to VQB-subscribe@egroups.com
(check website)



