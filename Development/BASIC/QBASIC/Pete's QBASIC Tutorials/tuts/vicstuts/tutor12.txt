
                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XII

                              BASIC ASM!!!
               Don't let it scare you!! Your bigger than it...

-----------------------------------------------------------------------------

When someone talks about assembly what do you think?  Do you think "Woah! 
ASM is way out of the reach for me!!" ?  Or do you say "hmm. that sounds
interesting, how do I learn..."?  The correct answer is the second one.  Well
I guess it should be, but its up to you... If you think the first one when 
ASM is mentioned than, well, Don't...  I guess...

The code itself is very confusing, thats because it was written for a
computer and doesn't allways come from your brain.  I will be the first idiot
to tell you that ASM is in some ways just like QB...  But it matters how you
look at it...

Where to start...

I'll give you an example first...

push ax
mov ax,13
int 10h
pop ax

beleive it or not, that would be the equivalent of the QB command
SCREEN 13

but look at this...

push ax
mov ax,1
int 33h
pop ax

that turns the mouse pointer on...
not a big difference huh?
about the same ammount of code and it does something completly unrelated...
WTF?  How is that posible?  

------------------
note:  You don't really need the push ax and pop ax, but I'm sure some ASM
       programmers who just happend to skim over this tutorial for mistakes
       would send a thousand E-mails on how stupid I am...
------------------

Qbasic does have the SCREEN 13 Command, but it doesn't have any mouse
controll.  If its so easy, why not?  I honestly don't know... but, who cares.
Where Microsoft fails we will suceed! or is it succeed? suck seed sounds
better...  Let me rephrase that ...

Where Microsoft fails we will suck seed!  on second thought, that sounds a
bit disgusting...  maybe it should be the other way around... ...
Where was I???

hmmm...

Thats what you get for writing every tutorial in NOTEPAD.exe...

OK OK!!!

-----

How do I know what the above examples do?
Easy...  I look them up...
Where do I look them up you ask???
Well, the best Idea to look them up is in a book of Interupts...
but, if you are poor or lazy, (or lazy and poor like me) you can find
a site with (almost?) all the interupts for an IBM computer...
Where?  well, at the current date you can find this page at this website...

http://www.cs.cmu.edu/afs/cs.cmu.edu/user/ralf/pub/WWW/files.html

I can't thank the guys/girls that keep up these files enough!!!
I owe them a lot...

But, I would really suggest getting a book...
ok... back to the tutorial...

-------

What is an interupt?


When an interupt is called the computer is told to do something based on
what the regesters tell it to do...

Intterupts are called by the INT command...

Ok, whats a regester?

a regester is a place to store variables...
some regesters are

AX
BX
CX
DX
...

registers values are changed by the MOV (move)
The mov command works like this

mov  Destination, source (or) # to go into Destination
mov  register, " "

so, in this command

mov ax,1

ax now equalls 1

this is the equivalent of

let ax = 1
in QBasic

-----
note:  If you see something like 53h or CFh it is the same as 53 and CF, the
       h is ignored...
-----

-----------------------------------------------------------------------------
ALLRIGHT! we have our first two ASM commands...
MOV & INT

Now what?

-0-0-0-

Lets try to find out how to turn on the mouse...
Ignoring the above example...!


--
Ok, First I get out the list of Intterupts and look through until I see
the intterupt that works the mouse...   
I look through it, and find that it is Interupt 33...

here is an exact copy of what I saw...












-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
--------M-330000-----------------------------
INT 33 - MS MOUSE - RESET DRIVER AND READ STATUS
	AX = 0000h
Return: AX = status
	    0000h hardware/driver not installed
	    FFFFh hardware/driver installed
	BX = number of buttons
	    0000h other than two
	    0002h two buttons (many drivers)
	    0003h Mouse Systems/Logitech three-button mouse
	    FFFFh two buttons
SeeAlso: AX=0011h,AX=0021h,AX=002Fh,INT 62/AX=007Ah,INT 74
--------M-330001-----------------------------
INT 33 - MS MOUSE v1.0+ - SHOW MOUSE CURSOR
	AX = 0001h
SeeAlso: AX=0002h,INT 16/AX=FFFEh,INT 62/AX=007Bh,INT 6F/AH=06h"F_TRACK_ON"
--------M-330002-----------------------------
INT 33 - MS MOUSE v1.0+ - HIDE MOUSE CURSOR
	AX = 0002h
Note:	multiple calls to hide the cursor will require multiple calls to
	  function 01h to unhide it.
SeeAlso: AX=0001h,AX=0010h,INT 16/AX=FFFFh,INT 62/AX=007Bh
SeeAlso: INT 6F/AH=08h"F_TRACK_OFF"
--------M-330003-----------------------------


=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=







If you look close, it tells you how to reset the mouse, turn on the mouse and
how to hide the mouse!!!  I don't think there is any way that this would just
pop into anyones head!  you have to look this crap up...  So don't feel at
all like you are cheating...


ok, in the beggining it says...


---
INT 33 - MS MOUSE - RESET DRIVER AND READ STATUS
	AX = 0000h
---

right???

guess what this does... it RESET DRIVER AND READ STATUS for the MSMOUSE
            (thats the mouse that everyone on a pc uses...)

Ok, we understand the INT 33...
But whats the AX = 0000h?

That says before you call INT 33 you have to make the AX register equall
0000h (that equals just 0)

Remember the command to put a number in AX? MOV right?
So, to RESET THE MOUSE DRIVER we put this...



MOV AX,0000h
INT 33


There!!! the mouse is reset!!!
we could also say it like this

MOV AX,0
INT 33


Ok, what does it say next?


----

INT 33 - MS MOUSE v1.0+ - SHOW MOUSE CURSOR
	AX = 0001h
----

Guess what this does...  you get the picture...
in this example AX = 0001h which is the same as 1 (just take off the 0's and
h's)

As you could guess, this is what you would put to turn the mouse on...

MOV AX,0001h
INT 33

BANG!! The mouse is on the screen!



----
NOTE:  These programs are in asm not QBASIC...  you need an assembler to make
       these programs work...
----

OK, I will leave the following space empty for you to show how to Hide the
mouse pointer...  Fill in the blanks...


MOV AX, -blank-
INT -blank-


----------------------------------------------------------------------------

I know for the past few programs you have just been thinking of how they
work, and not actually watching them work for yourself...  Lets get one
to work...

For this ASM I will Use a free assembler that everyone has on their computer.
Its called DEBUG... yeah, you know what I'm talking about...  It's one of
those old DOS fossil programms that make their home in the
c:\windows\command folder...
Why does it still come with WINDOWS?  I have no Idea, But its free!!!!

OK!!

Lets first figure out a program to run...
How about changing the screen in DOS?  Great!! I knew you would agree!!!

Ok,  I looked through the Interupt list And I found this...



--------
INT 10 - VIDEO - SET VIDEO MODE
	AH = 00h
	AL = desired video mode (see #00010)

!!further down the file I find this under #00010...!!

13h = G  40x25	 8x8   320x200	256/256K  .   A000 VGA,MCGA,ATI VIP
--------

hmmm. 320 x 200 pixels with 256K screen thingy or whatever its called...
doesn't that sound familiar?  HA! I got it! ITS SCREEN 13!!!!

OK just to let you know, AX is split into 2 parts. AH and AL...
And since AH = 00 then you only need to put the number into AX... Confused?

So, here is the program...

MOV AX,13
INT 10
MOV AX,4C
INT 21


ok, the mov ax,4c and INT 21 just tell the program to stop...  If you don't
stop the program, it will never end and will end up in an error and crash...
just make sure you put that at the end of your program...

Now, lets make this program work!!!!


Open up a dos window...  (run command.exe)
In this window TYPe

DEBUG

you should then be greated by this...

-

That means you are in DEBUG.exe  (GREAT!!!)

OK, type

a


this puts you into assembler mode...

you should see this now...


197A:0100

or something like that...

Now, type the first line of the program...

MOV AX,13

and then hit enter...

now enter the next line...

INT 10

Hit enter...

Now, type

MOV AX,4C

hit enter then type

INT 21

Now, hit enter...
Now, Hit enter once agian, you should then see the

-

type 

n scr13.com

then hit enter...

now type

RCX

and hit enter...

Now, type this...

A

and hit enter...

now type this...

w

and hit enter

and then type

q
and hit enter


And thats it!!!
You just made your first ASM program.. not that hard huh?

ok, just as a final check, this is what your entire DOS screen should say...


!@!@!@  MAKE SURE YOU DON'T CLOSE THIS DOS WINDOW!!!


-----------------------------------------------------

Microsoft(R) Windows 95
   (C)Copyright Microsoft Corp 1981-1996.

C:\WINDOWS>debug
-a
197A:0100 mov ax,13
197A:0103 int 10
197A:0105 mov ax,4C
197A:0108 int 21
197A:010A
-n scr13.com
-rcx
CX 0000
:A
-w
Writing 0000A bytes
-q

C:\WINDOWS>


-----------------------------------------------------

!@!@!@  MAKE SURE YOU DON'T CLOSE THIS DOS WINDOW!!!

If your DOS session doesn't look like this, then you might have messed up...
besides the Microsoft(R)... crap
and in some cases the 197A:0100...

OK!
Now what?
We have just made a program called SCR13.com...

!@!@!@  MAKE SURE YOU DON'T CLOSE THIS DOS WINDOW!!!

In the same dos window type the name of the program...
So, type 

scr13

Look what happens!!  The screen mode is changed from the normal screen 0 to
screen 13!!  Isn't that weird? 

Hey, do want to do something even more weird?
Ok! great!
in the same DOS window type this in order... 
!!! make sure the DOS window is maximized... if its not, hit   ALT & ENTER


debug {enter}
a {enter}
mov ax,1 {enter}
int 33 {enter}
mov ax,4C {enter}
int 21 {enter}
{enter} don't type the word Enter just hit enter!!
{enter}
g {enter}

----
When you hit enter after the g the program is run... and look what it does!
It shows the mouse,  A mouse in a dos window? WTF?

If you look closely you can see the program that is run

mov ax,1
int 33

Remember what that does?  obviously...

OK!! Now you know some ASM!!!  I bet you thought you would never get
it ever...  To tell you the truth, ASM does get harder as you get into the
more complex things like loops and stuff,  But You can learn it...

---

In the next tutorial, I plan to tell you how you can use this in QBASIC...
I think that will be a very small tutorial, It is very easy to do...






-----------------------------------------------------------------------------
*****************************************************************************
-----------------------------------------------------------------------------

Thats it for this tutorial, If I didn't get into enough detail in the
explanations then just look at the source code and try to figure it out
on your own.  All else fails E-Mail Me...

My current E-Mail address is RADIOHANDS@AOL.com

If you are using this tutorial on your page, please leave the tutorial 
exactly as it is... please don't change anything, unless its spelling
errors... Theres alot of them! I don't like using the backspace key...

The original website that these were on is

http://members.aol.com/radiohands/index.html

Thank you
Vic Luce
Finished
January 13
2000

If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)



