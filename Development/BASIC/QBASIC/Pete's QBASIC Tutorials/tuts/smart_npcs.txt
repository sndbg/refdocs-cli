Smart NPC's

------------------------------------------------------------

   * Subject: Smart NPC's
   * From: dancer@brisnet.org.au (root)
   * Date: 29 Dec 1995 23:38:59 GMT
   * Newsgroups: rec.arts.int-fiction
   * Organization: Brisnet Public Network Access

------------------------------------------------------------

A while ago someone posted asking why we didn't have more 'believable'
NPCs. I made a bit of an explanatory post with regard to that, which
appears to have fallen into the bit bucket. Since I don't keep copies of
these things (normally), this is both rewrite and repost.

There will be a couple exercises for the reader, too. Grab a pencil and
a sheet of paper.

                'Smart' NPC's in Interactive Fiction
                (What we can and can't do, and why)

I can skip the obligatory definitions of IF, the player and so forth, so
let's go straight to the NPC. The NPC is some animate agent filling the
role of some other character in an IF story. (Black in Jigsaw; Edward
in Christminster; Floyd in Planetfall)

What do they do? (Not what function do they serve, that's for a
different newsgroup entirely)

An NPC responds to her surroundings in a consistant, and intelligent
way. That's the nub of it. Floyd (Planetfall) tried to make sensible
responses to player questions and speech, passed comment on anything
noteworthy that occurred and so forth.

However, IF NPC's are little more than random (or scheduled) machines
with stimulus-response mechanisms. (There is contention as to whether
humans are any more than this, but just several orders of magnitude more
complex, but that's a little out of our scope)

Now, we have NPC's that do this, but find them a little less than
satisfying. We want the 'Smart NPC'. This NPC does the same thing as the
one above, only more so, so let's break it down a little:

* Has goals.
* Seeks to accomplish those goals.
* Knows things.
* Remembers things.
* Can speak about what it knows or remembers.
* Can understand complex player input (like this sentence).

Let's look at a simple goal. Our NPC is in the attic, looking for an old
photograph. There's the usual paraphenalia of tea-chests, cardboard
boxes and locked trunks and suitcases.

Now, how does our NPC accomplish that goal? How would you do it, as an
IF player?

Exercise: Write down a set of actions, that you (the IF player) would
type to look for the photograph.
............
By now, we should have a lot of actions. How did you arrive at
these actions? You 'know' that objects can be inside certain other kinds
of objects (inside a box, or a barrel, but probably not inside a table)
or under or behind things. Currently, we have an action list that our
NPC can perform, to look for the photograph. What if the photograph
hasn't been found at the end of it? More actions in the list, then.

Can we specify more general rules so that we don't _need_ a large action
list? Let's write some simple rules:

Exercise: Attempt to write a set of simple rules to generate the same
kind of actions that are in the action list. An example:

        If the object can contain things then
                look inside it.

............
How did we do? Did you remember to look _under_ things? Behind them?
Now, we'll give these instructions to Virtual-Joe. Joe is our NPC.

Joe is here. Joe is looking for something.
> WAIT
Joe looks in the large tea-chest, and rummages for a minute. He shakes
his head and then looks around.
Joe is here. Joe is looking for something.
> WAIT
Joe looks under the sharp needle. "Where is that damn photograph," he mutters.

Oops. A needle is too small to conceal a photograph. But Joe doesn't
know that. We didn't tell him.

        if the object is larger than the photograph then
                look under it.

Hmm. We're missing something though. Cupboard, needle, and photograph
are just words. Joe doesn't know what is larger than what. So, we'll
have to make a list.

needle.size=1;
loose-marble.size=2;
photograph.size=3
teachest.size=12
trunk.size=15
box.size=9

Okay. Joe can now tell that he can't find the photograph under the
marble or the needle. This is good. We're not out of the woods, yet,
though...

Joe is here. Joe is looking for something.
> DROP BREADBOX
Joe looks under the breadbox, muttering.
Joe is here. Joe is looking for something.

See? We can keep lugging stuff up here, and Joe will dutifully look
under each thing that we drop that is large enough to conceal a photo.
Moreover he will also look inside it, if it's the sort of object that
can contain things.

Worse:
Joe is here. Joe is looking for something.
> GET STEAMER TRUNK
You heave the large steamer trunk up off the floor.
Joe looks in the large tea-chest, and rummages for a minute. He shakes
his head and then looks around.
Joe is here. Joe is looking for something.
> DROP IT
With a grunt, and a sigh of relief, you lower the trunk to the floor.
Joe looks under the large steamer trunk.

Joe has to be equipped with some kind of memory here. The photograph is
obviously not under the trunk (because we lifted it up). We need to tell
Joe not to try looking under it, in that case. What if we empty the box?
Will he still try to look inside it? Should he? How do we prevent this?

In the AI world, this field is called knowledge representation. We have
to find methods of representing these little bits of information so that
we can make them available to our behavioural 'rules' (which we did
earlier).

Now, presume that the steamer trunk is locked. How does Joe go about
solving this? Does he start looking for a key? Boltcutters? Hacksaw?
Or does he grab a Yellow Pages and call a locksmith? Does he just give
up?

The coder has to decide. In fact the coder has to be aware of everything
that can happen (at least in a general sort of way) or the game and
characters will not respond appropriately.

> GIVE PAPERWEIGHT TO JOE
Joe looks at the glass paperweight, turns it over and shrugs. "I
can't see this being a terrible lot of use just now." he says.
Nevertheless, he puts it in his pocket.

> GIVE BROKEN GLASS TO JOE
Joe looks at the collection of sharp shards, turns it over and shrugs. "I
can't see this being a terrible lot of use just now." he says.
Nevertheless, he puts it in his pocket.

(Ouch)

Here's a classic knowledge representation problem, in fact. In order to
respond sensibly to each object in the game that might be given to Joe,
the coder has to sit down and write a response for each object, _or_ try
to add properties or rules to classify responses among them. We might
use one flag to mark objects as 'possibly useful', another as 'not the
sort of thing you want in your pocket', and so on.

The problem is (as in the field of AI) that even in a limited
environment, your flags, properties and rules outweigh your objects by
a factor of several thousand or more, and you still don't have enough
information to discuss or manipulate the objects in any fashion that a
moron wouldn't find too basic.

(Hmm..160-something lines already. I'll post this chunk as is. Lemme
know if you want more of it)