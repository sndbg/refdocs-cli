                      ......................   
                     /                      \   
                    /      KEYS TUTORIAL     \
                   /   By Meech Productions   \
                  /  Reply to mooch23@msn.com  \
                  \----------------------------/

If the text is flying off the screen, go to Edit, then Word Wrap!

==============
 Introduction
==============

Hello all! For all you novices in QB out here reading this, have you ever wondered how to use keys such as the directional keys or letters on the keyboard in QB? I was like that one time... And I was very frustrated. After all these good tutorials out there, how could they miss such an important thing!? Well if you have come here finding how to use this in QuickBasic, I have the remedy.  

=================
 INKEY$ Commands
=================

Let's start with something fairly simple. INKEY$ commands. Have you ever wanted to prompt the user with "press any key to continue?" but you cannot because you only now how to use numbers? Of course you have! Except sometimes people have trouble finding the 'any' key. I still can't find it to this day, where the heck is the darn ole' any key anyways?(Okay, I didnt make that up, I got it from the Simpsons) 

PRINT "Press any key to proceed."
DO
LOOP WHILE INKEY$ = ""

If any key is pressed, it will continue!(Or the program will end in this case.)  The DO ignites the LOOP telling the program to do something over and over WHILE the INKEY$(which is kind of like a "hotkey") equals "". Since there is nothing in the quotes, any key can be pressed for Qbasic to move on.

Hmmm...But the doesn't give you a whole lot does it? Don't worry, we will get to the part where you learn letters...

===================
 UCASE$ and LCASE$ 
===================


If you haven't guessed yet, UCASE$ stands for upper case and LCASE$ stands for lower case.

Lets start off with a friendly example.

CLS 'clears the screen
PRINT "Press S to start the program, or E to end."
INPUT choice$
IF UCASE$(choice$) = "S" THEN GOTO label
IF UCASE$(choice$) = "E" THEN PRINT "Thanks for using bro!" 
END 

Looks pretty simple doesn't it? You could probably figure this out with out me having to explain it to you. First off, you must INPUT the user for a string$. Using if then statements is what I did here, then you want the UCASE$ to be equal to whatever letter. After that make sure you included the string next to what you INPUTted the user for. Also, the string must be in parentheses. Next make the UCASE$ = to a letter, and make sure the letter is in quotes or it won't work. It also has to be capitals, since we are using UCASE$. Then continue on whatever you want to do within the IF...THEN statements. When the program opens up the user will press 'S' to start and/or 'E' to end. Please remember that the user doesn't have to use capitals or lowercase while inputting a letter. See, aren't you glad you know this now? I like oatmeal raisin cookies please :D

Alternatively, let's see what we can do while using LCASE$.

shop:
CLS
PRINT "Mooch's Magical Shop!"
PRINT "Press p to buy a potion"
PRINT "Press e to buy an ether"
PRINT "Press l to leave."
INPUT choice$
IF LCASE$(choice$) = "p" THEN PRINT "You bought a potion and restored your 
PRINT "health by ten!"
IF LCASE$(choice$) = "e" THEN PRINT "You bought an ether and restored your  print "magic points by ten!"
IF LCASE$(choice$) = "l" THEN GOTO label
DO
LOOP WHILE INKEY$ = ""
GOTO shop
label:
PRINT "YOU'RE LEAVING!? GAAAAH! HOW MANY STORES DO YOU SEE THAT CARRY RPG 
PRINT "ITEMS!? EH!?"
DO
LOOP WHILE INKEY$ = ""
END

The following program is pretty simple. The only difference is when you use LCASE$ in these IF THEN statements, the letter in the quotes has to be lowercase, instead of upper case. Once again, when the user is buying or leaving the letters he/she inputs are not case sensitive. Use which ever command fulfills your needs more. But if both UCASE$ and LCASE$ are case insensitive, then why not just have the LCASE$ command only? We will go over that after one more example...

In this example, I plan on showing you how you could go by without using either UCASE$ or LCASE$ for using a password feature in your programs.

CLS
PRINT "Enter the password to enter the top secret database."
PRINT "CAUTION: Passwords are case sensitive!"
INPUT "Enter Password ", choice$
SELECT CASE choice$
CASE "MOOCH"
COLOR 10
PRINT "Correct Password!"
GOTO label
CASE ELSE
COLOR 4
PRINT "THAT IS THE WRONG PASSWORD, THE BUILDING WILL SELF DESTRUCT IN 30 PRINT "SECONDS!!! "RUN!!! OR YOU'RE FEET WILL EXPLODE!"
END
END SELECT

In this example, the program is case sensitive. The password must be MOOCH or the building will self destruct, and....er....*GASP* Your feet will explode! So make sure the password is in capitals! The same rules would apply if the password was "mooch" or "mOOch"

^Passwords like that are very handy. You can use that in certain games, give the user passwords after they beat certain levels. For instance, once the program loads it would prompt the user for a password to a level in the game, and then that password would GOTO to a label which goes to the level which the player is on.

Okay, now on to what I said I was going to explain about UCASE$ and LCASE$. I am not sure if this belongs in this tutorial, but for chuckles and giggles you can have a peek of what makes UCASE$ and LCASE$ distinctive.

name$ = "chicago bulls"
PRINT UCASE$(name$); " WIN!"   

See what it does? Even though the string name$ is in all lower case letters, using the command PRINT UCASE$ prints the whole string in upper case! And of course, it works with LCASE$ to.

name$ ="UTAH JAZZ"
PRINT LCASE$(name$); "....win...BOOO"

Sorry Utah...I guess you where the underdog this time...

=========================
 USING DIRECTIONAL KEYS
=========================

This isn't a directional key, but I am going to start off with how to use the escape key to exit a program.

INKEY$ = press$ 
IF press$ = CHR$(27) THEN END

You have to set the INKEY$ in another string..(Well you don't HAVE TO but it makes things a lot easier, in my opinion atleast.) If press$ = CHR$(27)(That is the escape key number 27, to see a list of the ASCII character codes, go to help, contents, and select it off the menu) THEN the program will END.

Now to the cool part........Directional Keys! YAY! These could be used in a whole lot of things..such as games, and menus...

 
Here's an example of how you move a letter around the screen. I hope this example isn't too advanced to start with...

row# = 10
column# = 10
CLS 'clears the screen
LOCATE row#, column# 'locate places text on a certain place of the screen
PRINT "X"
DO
move$ = INKEY$
IF move$ <> "" THEN
SELECT CASE move$
CASE CHR$(0) + CHR$(72) 'The up key! ^_^
CLS
row# = row# - 1
LOCATE row#, column#
PRINT "X"
PRINT "You pressed up!"
CASE CHR$(0) + CHR$(80) 'The down key... ;_;
CLS
row# = row# + 1
LOCATE row#, column#
PRINT "X"
PRINT "You pressed down!"
CASE CHR$(0) + CHR$(75) ' The left key. <_<
CLS
column# = column# - 1
LOCATE row#, column#
PRINT "X"
PRINT "You pressed left!"
CASE CHR$(0) + CHR$(77)
CLS
column# = column# + 1
LOCATE row#, column#
PRINT "X"
PRINT "you pressed right!" 'The right key. >_>
CASE CHR$(27)
END
END SELECT
END IF
LOOP



Heh, using LOCATE commands and variables this example makes the letter X move around the screen! If the X goes off the screen it will result in an illegal function call.

But what syntax did I use to make the X go left, right, up, down? CHR$ again.

CHR$(0) + CHR$(72) is used for up.
CHR$(0) + CHR$(80) is used for down.
CHR$(0) + CHR$(75) is used for left.
CHR$(0) + CHR$(77) is used for right.

You must use "CHR$(0)+ " when using DIRECTION KEYS for doing stuff like this!

Make sure to include something like 

press$ = INKEY$
DO

before the direction commands and LOOP after. 

Next, I CLSed the screen every time a direction key was pressed. According to which key was pressed, I would either subtract or add 1 to the column or row.
It is very important to know that rows go up and down and and columns go left and right. Then I would use the command LOCATE row#, column# to place the X at a certain place on the screen. LOCATE is different from how you would place graphics, graphics are placed "PSET x(left right), y(up down)" and text is placed "LOCATE row(up down), column(left right)"

Here's another way of doing it but using IF...THEN statements instead!(This is a program that shall review what you have learned so far)

color1# = 7 'color 7 
CLS
PRINT "Press S to start program"
PRINT "Press E to not start program..."
INPUT choice$
IF LCASE$(choice$) = "s" THEN GOTO prog
IF LCASE$(choice$) = "e" THEN PRINT "FINE THEN! DONT TRY THE LAST EXAMPLE"
PRINT "IN THIS TUTORIAL!"
END
prog:
CLS
DO
press$ = INKEY$
LOCATE 1, 1
COLOR color1#
IF press$ = CHR$(0) + CHR$(72) THEN PRINT "^_^ BOB LOOKS UP!   "
IF press$ = CHR$(0) + CHR$(80) THEN PRINT "v_v BOB LOOKS DOWN! "
IF press$ = CHR$(0) + CHR$(75) THEN PRINT "<_< BOB LOOKS LEFT! "
IF press$ = CHR$(0) + CHR$(77) THEN PRINT ">_> BOB LOOKS RIGHT!"
IF press$ = CHR$(13) THEN PRINT "Bob disappeared!              " 'enter key
IF press$ = CHR$(32) THEN PRINT "Bob re-appeared!              " 'space key
IF press$ = CHR$(27) THEN END
IF press$ = "b" THEN color1# = 1 'blue
IF press$ = "g" THEN color1# = 2 'green
IF press$ = "c" THEN color1# = 3 'cyan
IF press$ = "r" THEN color1# = 4 'red
IF press$ = "m" THEN color1# = 5 'magenta
IF press$ = "o" THEN color1# = 6 'orange
IF press$ = "g" THEN color1# = 7 'gray
IF press$ = "d" THEN color1# = 8 'dark gray
IF press$ = "l" THEN color1# = 9 'light blue
IF press$ = "q" THEN color1# = 10'light green..but lets call it qwazy green
IF press$ = "s" THEN color1# = 11 'light cyan..but lets call it super cyan
IF press$ = "e" THEN color1# = 12 'light red..but lets call it energetic red
IF press$ = "p" THEN color1# = 13 'pink
IF press$ = "y" THEN color1# = 14 'yellow
IF press$ = "w" THEN color1# = 15 'white
LOCATE 20, 1
PRINT "Color "; color1#
LOOP

This program makes Bob look in what ever direction you press! If you press a certain letter it changes the color by setting a number 1-15(all the colors except black)in the color1# variable, and at the top the color command COLOR color1# is used. You also learned two new ASCII character codes, CHR$(13)(Enter) and CHR$(32)(Space). 


==========================
 Legal Notice/Conclusion
==========================

Surely you have noticed that I have you used lots of code in my program examples....There probably are shorter ways people would make these in QB. However, I like using these ways because I believe they are the easiest to understand!

You may also want to memorize these CHR$ codes

CHR$(0) + CHR$(72) is used for up.
CHR$(0) + CHR$(80) is used for down.
CHR$(0) + CHR$(75) is used for left.
CHR$(0) + CHR$(77) is used for right.
CHR$(13) is used for enter.
CHR$(32) is used for space.
CHR$(27) is used for escape.


I find those codes the most important, but of course there are plenty more that you can be used. To find them goto the help files, select contents and chooser the "ASCII Character Codes" hyperlink at the bottom. 

Well, there you go, you should have no problem with using key commands! I hope... Best regards and good luck with programming with these in QB... And I hope my awfully *COUGH* funny jokes at times didn't make you lose your voice from laughing too hard...

Comments or questions are very much welcome! Send them to the author at mooch23@msn.com !

Now, feel free to print a copy of this, make origami, or feed it to your goat! But I do take some pride in my work, so you definetly may not make any profits out of this, or claim it as your own! Not that you would want to... If you would like to distribute this or use it on your site or whatever, just drop me an email, and I'll be sure to let you! Thanks!

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
This guide is a copyright of Meech Productions written on 6-11-04.  