#######################################
# libdvdcss - Grab source and compile #
#######################################

You'll have to do this yourself for legal reasons, but you can get a copy
of the source code for libdvdcss by running:

    git clone https://code.videolan.org/videolan/libdvdcss.git
    
It's not that hard and works waaaaay better than grabbing a premade version.

Other source:

    https://download.videolan.org/pub/libdvdcss/last/

Thankfully, the source uses a classic "configure, make, make install."
