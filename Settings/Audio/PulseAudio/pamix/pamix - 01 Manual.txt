pamix(1)                                  pamix man page                                 pamix(1)

SYNOPSIS
       pamix [--version]

DESCRIPTION
       This is a pavucontrol inspired ncurses based pulseaudio mixer for the commandline

OPTIONS
   General options
       --version print build version

CONFIGURATION
       pamix  is  configured  using  a  file  called  pamix.conf  inside  the $XDG_CONFIG_HOME or
       $XDG_CONFIG_DIRS directories or their default values, should they not be set.
       $XDG_CONFIG_HOME will be preferred over $XDG_CONFIG_DIRS.

COMMANDS
       PAmix conf files support the following commands:
       * set * bind * unbind * unbindall

       characters after a ';' will be interpreted as comments and ignored

set
       SYNOPSIS: set KEY=VALUE

       set is used to set  a  variable.  This  is  currently  only  relevant  for  the  'pulseau‐
       dio_autospawn' and 'default_tab' options.

bind
       SYNOPSIS: bind KEYNAME MIXER-COMMAND [ARGUMENT]

       bind is used to bind a keyname to a mixer-command.
       Some mixer-commands require an argument.
       You can bind a keyname to multiple mixer-commands.

unbind
       SYNOPSIS: unbind KEYNAME

       unbind will remove all bindings for the given keyname

unbindall
       SYNOPSIS: unbindall

       unbindall will remove all key bindings

PAMIX-COMMANDS
       Pamix-Commands  can  be bound to keys using the bind command and are used to interact with
       pamix.
       The following pamix-commands are currently supported: * quit * select-tab * select-next  *
       select-prev * set-volume * add-volume * cycle-next * cycle-prev * toggle-lock * set-lock *
       toggle-mute * set-mute

quit
       quit will cause PAmix to exit and takes no arguments.

select-tab
       select-tab will select one of the following tabs: Output Devices, Input Devices, Playback,
       Recording, Cards
       select-tab takes the number of the tab to switch to starting at 0 in the order mentioned.

select-next and select-prev
       these  commands  are  given  the optional argument 'channel' they will select the next and
       previous channels.  if no argument is given they will select the next and  previous  entry
       in the displayed tab.

set-volume
       this command takes the targetvalue in form of a double as an argument.
       depending on weather channels are locked, this command will set the volume of the selected
       entry/channel to the targetvalue given in the argument.
       Example: bind 0 set-volume 1.0 ; this will set the volume to 100%

add-volume
       this command takes a deltavalue in form of a double as an argument.
       the deltavalue can be negative Example: bind h add-volume -0.05 ;  this  will  reduce  the
       volume by 5%

cycle-next and cycle-prev
       these commands will change the device or port of the currently selected entry.
       they dont take any arguments.

toggle-lock
       this command toggles weather channels should be locked together for the currently selected
       entry
       and takes no arguments.

set-lock
       this command takes either '0' or '1' as an argument and sets  the  channel-lock  like  the
       toggle-lock mixer-command.

toggle-mute
       toggles weather the currently selected entry is muted
       and takes no arguments.

set-mute
       works  like  the  set-lock mixer-command, but sets weather the currently selected entry is
       muted or not

DEFAULT CONFIGURATION
       pamix does not autospawn pulseaudio by default

       Keybindings:
       F1      show Playback tab
       F2      show Recording tab
       F3      show Output devices tab
       F4      show Input devices tab
       F4      show Cards tab
       0-9     set volume to percentage (10%-100%)
       j/down  select next channel
       J       select next entry
       k/up    select previous channel
       K       select previous entry
       h/left  decrease volume
       h/right increase volume
       c       un/lock channels
       s/S     select next/previous device/port

V1.6-10-gea4ab3b                           05 Sep 2016                                   pamix(1)
