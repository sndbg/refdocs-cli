usage: wal [-h] [-a "alpha"] [-b background] [--backend [backend]]
           [--theme [/path/to/file or theme_name]] [--iterative] [-c]
           [-i "/path/to/img.jpg"] [-g] [-l] [-n] [-o "script_name"] [-q] [-r]
           [-R] [-s] [-t] [-v] [-e]

wal - Generate colorschemes on the fly

optional arguments:
  -h, --help            show this help message and exit
  -a "alpha"            Set terminal background transparency. *Only works in
                        URxvt*
  -b background         Custom background color to use.
  --backend [backend]   Which color backend to use. Use 'wal --backend' to
                        list backends.
  --theme [/path/to/file or theme_name], -f [/path/to/file or theme_name]
                        Which colorscheme file to use. Use 'wal --theme' to
                        list builtin themes.
  --iterative           When pywal is given a directory as input and this flag
                        is used: Go through the images in order instead of
                        shuffled.
  -c                    Delete all cached colorschemes.
  -i "/path/to/img.jpg"
                        Which image or directory to use.
  -g                    Generate an oomox theme.
  -l                    Generate a light colorscheme.
  -n                    Skip setting the wallpaper.
  -o "script_name"      External script to run after "wal".
  -q                    Quiet mode, don't print anything.
  -r                    'wal -r' is deprecated: Use (cat
                        ~/.cache/wal/sequences &) instead.
  -R                    Restore previous colorscheme.
  -s                    Skip changing colors in terminals.
  -t                    Skip changing colors in tty.
  -v                    Print "wal" version.
  -e                    Skip reloading gtk/xrdb/i3/sway/polybar
