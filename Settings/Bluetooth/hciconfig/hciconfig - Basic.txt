#####################
# hciconfig - Basic #
#####################

Display Bluetooth device 'hci0' information:
-------------------------------------------

    hciconfig -a hci0

Enable/disable Bluetooth device 'hci0':
--------------------------------------

    sudo hciconfig hci0 up/down
    
Display Bluetooth device 'hci0' features:
----------------------------------------

    hciconfig hci0 features

In most cases, when using Bluetooth, the default device will be labeled 
'hci0.' This is sort of like the default Ethernet and WiFi being labeled
as 'eth0' and 'wlan0.'
