usage: rainbowstream [-h] [-s STREAM] [-to TIMEOUT] [-tt TRACK_KEYWORDS]
                     [-fil FILTER] [-ig IGNORE] [-iot] [-24] [-ph PROXY_HOST]
                     [-pp PROXY_PORT] [-pt PROXY_TYPE]

optional arguments:
  -h, --help            show this help message and exit
  -s STREAM, --stream STREAM
                        Default stream after program start. (Default: mine)
  -to TIMEOUT, --timeout TIMEOUT
                        Timeout for the stream (seconds).
  -tt TRACK_KEYWORDS, --track-keywords TRACK_KEYWORDS
                        Search the stream for specific text.
  -fil FILTER, --filter FILTER
                        Filter specific screen_name.
  -ig IGNORE, --ignore IGNORE
                        Ignore specific screen_name.
  -iot, --image-on-term
                        Display all image on terminal.
  -24, --color-24bit    Display images using 24bit color codes.
  -ph PROXY_HOST, --proxy-host PROXY_HOST
                        Use HTTP/SOCKS proxy for network connections.
  -pp PROXY_PORT, --proxy-port PROXY_PORT
                        HTTP/SOCKS proxy port (Default: 8080).
  -pt PROXY_TYPE, --proxy-type PROXY_TYPE
                        Proxy type (HTTP, SOCKS4, SOCKS5; Default: SOCKS5).
