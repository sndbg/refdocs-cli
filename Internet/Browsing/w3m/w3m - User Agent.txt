####################
# w3m - User Agent #
####################

I highly recommend that you make a copy of ~/.w3m/config and rename the 
copy as 'w3m_Desktop.' Then edit the 'config' file's User Agent line as:

    user_agent Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1
    
This is because most websites know that most "mobile" devices do not have 
the same JavaScript capabilities as a desktop, aka the same issue you face 
when using w3m. This makes w3m much more compatible. However, some websites 
will either show an error or try to get you to download an app because it 
thinks you're using a smart phone. This is where the 'config_Desktop' comes in:

    w3m https://... -config ~/.w3m/config_Desktop
    
It would be must easier to open your ~/.bashrc file and add:

    alias w3m-desktop="w3m https://... -config ~/.w3m/config_Desktop"
    
Close the terminal or run 'exec bash' and then try opening the error 
website again with 'w3m-desktop https://...'

| Playing video
`--------------
There's also another advantage to using a mobile UA and that is playing 
embedded video on youtube-dl supported websites. This is because mobile 
versions of websites will typically load the MP4 versions of video instead 
of FLV, which most media players have an easier time with. Open w3m to any 
website and press 'o' to open settings. Navigate to 'External browser' and 
change it to the following:

    mpv --vo=opengl,drm,x11,tct,caca --ao=pulse,alsa --ytdl-format="best[ext=mp4][height<=?720]"
    
This will cycle through all possibilities that mpv can handle. Note that 
if using a Raspberry Pi, you're better off putting x11 to the front; this 
is also why I have my best height set to 720 because RPi does not handle 
anything larger than that very well at all.
