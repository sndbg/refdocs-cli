From: https://linux.die.net/man/8/wicd-curses

die.net

wicd-curses(8) - Linux man page

Name

wicd-curses - curses-based wicd(8) controller

Description
-----------

wicd-curses is a curses-based network controller that uses the Wired/Wireless
Internet Connection Daemon (wicd) to control your network connections. It is
suitable to run in terminal multiplexers like screen(1).

It is designed to imitate the GTK-based wicd-client(1) as much as possible, and
uses the Urwid (http://excess.org/urwid) console widget library to vastly
simplify development.

This man page only documents the current status of wicd-curses. This may/may
not be the most up-to-date document.

Controls
--------

All of these are case sensitive.

C or enter
    Connect to selected network
F8 or Q or q
    Quit the client.
D

    Disconnect all devices from network connections

    ESC

    If connecting to a network, stop doing so
F5 or R
    Refresh the network list
P

    Bring up the preferences controller

    I

    Bring up hidden network scanning dialog
H or h or ?
    Bring up a rather simplistic help dialog. Of course, it mentions this man
    page first. :-)
A

    Raise the "About wicd-curses" dialog
right-arrow
    Bring up network configuration controller for the selected network
delete

    Delete the selected wired network profile (from the wired network combo box
    at the top)

    F2

    Rename the selected wired network profile (from the wired network combo box
    at the top)

    S

    Bring up instructions on how to edit the scripts. I have implemented a way
    to do this in the interface itself, but making it function with all Linux
    distros would be difficult. Since you are reading this, you should know how
    to do what I suggest. ;-)

    O

    Raise the Ad-Hoc network creation dialog

See Also

wicd-client(1), wicd(8)

Bugs

Probably lots. ;-)

If you happen to find one, ask about it on #wicd on freenode, or submit a bug
report to http://launchpad.net/wicd.

Wicd-curses Author

Andrew Psaltis <ampsaltis@gmail.com>

Wicd Authors

Adam Blackburn <compwiz18@gmail.com>
Dan O'Reilly <oreilldf@gmail.com>

Referenced By

wicd-manager-settings.conf(5), wicd-wired-settings.conf(5),
wicd-wireless-settings.conf(5)
