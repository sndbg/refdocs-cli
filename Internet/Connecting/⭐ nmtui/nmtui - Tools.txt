#################
# nmtui - Tools #
#################

nmcli - is a command-line tool for controlling NetworkManager and getting its status.

nm-online - is a utility to find out whether you are online.

nmtui - is an interactive ncurses based interface for nmcli.

nmtui-connect - is an interactive ncurses based interface connection activate/deactivate.

nmtui-edit - is an interactive ncurses based interface connection editor.

nmtui-hostname - is an interactive ncurses based interface hostname editor.
