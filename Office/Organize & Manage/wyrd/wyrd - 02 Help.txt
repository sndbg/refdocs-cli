Usage: wyrd [OPTIONS] [FILE]
Open a front-end to remind(1) using FILE as the reminders file.

OPTIONS:
  --version  Display version information and exit
  -a         Add given event to reminders file and exit
  --add      Add given event to reminders file and exit
  -help      Display this list of options
  --help     Display this list of options
