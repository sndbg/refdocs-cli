#####[ pandoc - Simple ePub Book ]#####

| Template example:
`------------------

% Book Title
% Author Name
% &copy; 2019 Publisher Name

# Chapter One

## Subtitle

Type some text...

------------------------------------------------------------------------
Essentially, you are using markdown to write this ePub book.
------------------------------------------------------------------------

Now in a command-line, use:

    pandoc book.markdown --epub-cover-image="cover.png" -o book.epub
