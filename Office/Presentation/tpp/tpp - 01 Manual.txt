TPP(1)                                    User Commands                                    TPP(1)

NAME
       TPP - Text Presentation Program

SYNOPSIS
       tpp <OPTIONS> <FILE>

DESCRIPTION
       Tpp  stands  for  text presentation program and is an ncurses-based presentation tool. The
       presentation can be written with your favorite editor in a simple description  format  and
       then  shown  on any text terminal that is supported by ncurses - ranging from an old VT100
       to the Linux framebuffer to an xterm.

       It supports color, various output modes, sliding in text, command prompt, LaTeX conversion
       and more.

OPTIONS
       -h/--help display help

       -l output.tex input.tpp converts tpp slides into tex

       -x allow usage of "--exec"

       -v/--version display version number

KEYS
       space  display next entry within page

       space, cursor-down, cursor-right
              display next page

       b, cursor-up, cursor-left
              display previous page

       q, Q   quit tpp

       j, J   jump directly to page

       s, S   jump to the start page

       e, E   jump to the last page

       c, C   start command line

WRITING PRESENTATIONS
       The  tpp  presentation formats consists of normal text lines and special commands. Special
       commands are contained in lines that begin with "--" and the command name.

       See   /usr/share/doc/tpp/README.gz   for   complete   list   of   command    names,    and
       /usr/share/doc/tpp/examples  for  TPP presentation examples, including presentation slides
       that have been in real-life presentations.

VIM SYNTAX FILE
       In /usr/share/doc/tpp/contrib  you'll  find  a  syntax  file  for  the  vim  editor.   See
       /usr/share/doc/tpp/README.gz for installation instructions.

AUTHOR CONTACT
       Tpp was written by Nico Golde <nico@ngolde.de> and Andreas Krennmair <ak@synflood.at>

tpp 1.3.1                                   April 2007                                     TPP(1)
