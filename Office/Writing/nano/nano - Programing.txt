######################
# nano - Programming #
######################

There are MANY better editors out there for programming, but if you must, 
one of the big issues VIM and EMACS people have with nano is 
syntax highlighting. However, contrary to what most of those people are 
saying, nano does in fact support syntax highlighting. The issue is 
usually with the terminal itself or they don't put '#!/bin/bash,' or 
whatever the language is, at the top like you are supposed to because 
they assume the file extension is enough. You can find programming 
languages nano supports inside of '/usr/share/nano/.'
